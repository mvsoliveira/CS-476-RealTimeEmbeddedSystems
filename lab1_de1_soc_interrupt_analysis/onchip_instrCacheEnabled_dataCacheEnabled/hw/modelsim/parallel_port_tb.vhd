-------------------------------------------------------------------------------
-- Title      : Testbench for design "parallel_port"
-- Project    : 
-------------------------------------------------------------------------------
-- File       : parallel_port_tb.vhd
-- Author     :   <msilvaol@PCPHL1CT05>
-- Company    : 
-- Created    : 2017-03-06
-- Last update: 2017-04-05
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2017 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2017-03-06  1.0      msilvaol        Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

-------------------------------------------------------------------------------

entity parallel_port_tb is

end entity parallel_port_tb;

-------------------------------------------------------------------------------

architecture tb of parallel_port_tb is

  -- component generics
  constant N : integer := 32;

  -- component ports
  signal Clk        : std_logic := '1';
  signal nReset     : std_logic;
  signal Address    : std_logic_vector (31 downto 0);
  signal ChipSelect : std_logic;
  signal Read_en    : std_logic;
  signal Write_en   : std_logic;
  signal ReadData   : std_logic_vector (31 downto 0);
  signal WriteData  : std_logic_vector (31 downto 0);
  signal IRQ        : std_logic;
  signal ParPort    : std_logic_vector (N-1 downto 0);








begin  -- architecture tb

  -- component instantiation
  DUT : entity work.parallel_port
    generic map (
      N => N)
    port map (
      Clk        => Clk,
      nReset     => nReset,
      Address    => Address(1 downto 0),
      ChipSelect => ChipSelect,
      Read_en    => Read_en,
      Write_en   => Write_en,
      ReadData   => ReadData,
      WriteData  => WriteData,
      IRQ        => IRQ,
      ParPort    => ParPort);

  -- clock generation
  Clk <= not Clk after 10 ns;

  -- waveform generation
  WaveGen_Proc : process

    procedure av_clr is
    begin
      wait until Clk = '1';
      Address    <= (others => '0');
      ChipSelect <= '0';
      Read_en    <= '0';
      Write_en   <= '0';
      WriteData  <= (others => '0');
    end av_clr;

    procedure av_read (
      addr : std_logic_vector(31 downto 0)
      ) is
    begin
      wait until Clk = '1';
      Address    <= addr;
      ChipSelect <= '1';
      Read_en    <= '1';
      Write_en   <= '0';
      WriteData  <= (others => '0');
    end av_read;

    procedure av_write (
      addr : std_logic_vector(31 downto 0);
      data : std_logic_vector(31 downto 0)
      ) is
    begin
      wait until Clk = '1';
      Address    <= addr;
      ChipSelect <= '1';
      Read_en    <= '0';
      Write_en   <= '1';
      WriteData  <= data;
    end av_write;

  begin
    nReset <= '0';
    wait for 25 ns;
    nReset <= '1';
    -- clear avalon
    av_clr;
    -- write value to parallel port
    av_write(X"00000000", X"0000FFFF");
    -- checking value
    av_read(X"00000000");
    -- setting the upper-most byte
    av_write(X"00000001", X"FF000000");
    -- clearing the lower-most byte
    av_write(X"00000002", X"000000FF");
    -- checking value
    av_read(X"00000000");
    -- enabling interrupts in the least significant bit
    av_write(X"00000003", X"00000001");
    -- setting least significant bit
    av_write(X"00000001", X"00000001");
    av_clr;
    wait for 100 ns;
    -- clearing least significant bit & acknowledge irq
    av_write(X"00000002", X"00000001");
    av_clr;
    wait;
  --wait until Clk = '1';
  end process WaveGen_Proc;



end architecture tb;

-------------------------------------------------------------------------------

configuration parallel_port_tb_tb_cfg of parallel_port_tb is
  for tb
  end for;
end parallel_port_tb_tb_cfg;

-------------------------------------------------------------------------------
