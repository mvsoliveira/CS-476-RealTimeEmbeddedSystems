-------------------------------------------------------------------------------
-- Title      : Timer
-- Project    : 
-------------------------------------------------------------------------------
-- File       : timer.vhd
-- Author     :   <msilvaol@PCPHL1CT05>
-- Company    : 
-- Created    : 2017-04-10
-- Last update: 2017-04-10
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2017 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2017-04-10  1.0      msilvaol        Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity timer is
  port (
    -- Avalon interfaces signals
    Clk        : in  std_logic;
    nReset     : in  std_logic;
    Address    : in  std_logic_vector (1 downto 0);
    ChipSelect : in  std_logic;
    Read_en    : in  std_logic;
    Write_en   : in  std_logic;
    ReadData   : out std_logic_vector (31 downto 0);
    WriteData  : in  std_logic_vector (31 downto 0);
    IRQ        : out std_logic
    );

end entity timer;

architecture rtl of timer is

  signal cnt        : unsigned(31 downto 0);
  signal iRegEna    : std_logic;
  signal iRegIrq    : std_logic;
  signal iRegIrqEna : std_logic;
  signal iRQReg     : std_logic;




begin  -- architecture rtl

  main_p : process (Clk)
  begin
    if rising_edge(Clk) then
      if nReset = '0' then
        cnt        <= (others => '0');
        iRegEna    <= '0';
        iRegIrq    <= '0';
        iRegIrqEna <= '0';
      else
        -- counter 
        if iRegEna = '1' then
          cnt <= cnt + 1;
        end if;
        -- irq generation
        if iRegIrqEna = '1' and cnt = 0 then
          iRegIrq <= '1';
        end if;
        -- avalon
        -- avalon write
        if ChipSelect = '1' and Write_en = '1' then   -- Write cycle
          case Address(1 downto 0) is
            when "00"  => cnt        <= unsigned(WriteData(31 downto 0));  --rst
            when "01"  => iRegEna    <= WriteData(0);
            when "10"  => iRegIrq    <= WriteData(0);
            when "11"  => iRegIrqEna <= WriteData(0);
            when others => null;
          end case;
        end if;
        -- avalon read
        ReadData <= (others => '0');
        if (ChipSelect = '1' and Read_en = '1') then  -- Read cycle
          case Address(1 downto 0) is
            when "00"  => ReadData(31 downto 0) <= std_logic_vector(cnt);
            when "01"  => ReadData(0)           <= iRegEna;
            when "10"  => ReadData(0)           <= iRegIrq;
            when "11"  => ReadData(0)           <= iRegIrqEna;
            when others => null;
          end case;
        end if;
      end if;
    end if;
  end process main_p;
  IRQ <= iRegIrq;
end architecture rtl;

