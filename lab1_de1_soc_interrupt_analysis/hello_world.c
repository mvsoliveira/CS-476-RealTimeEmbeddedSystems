/*
 * "Hello World" example.
 *
 * This example prints 'Hello from Nios II' to the STDOUT stream. It runs on
 * the Nios II 'standard', 'full_featured', 'fast', and 'low_cost' example
 * designs. It runs with or without the MicroC/OS-II RTOS and requires a STDOUT
 * device in your system's hardware.
 * The memory footprint of this hosted application is ~69 kbytes by default
 * using the standard reference design.
 *
 * For a reduced footprint version of this template, and an explanation of how
 * to reduce the memory footprint for a given application, see the
 * "small_hello_world" template.
 *
 */

#include <stdio.h>
#include "io.h"
#include "system.h"
#include "sys/alt_irq.h"
#include "altera_avalon_timer_regs.h"
#include <inttypes.h>

static const uint32_t ADDR_CNT_VAL = 0x0;
static const uint32_t ADDR_CNT_ENA = 0x4;
static const uint32_t ADDR_IRQ_VAL = 0x8;
static const uint32_t ADDR_IRQ_ENA = 0xC;

static const uint32_t ADDR_PIO_VAL = 0x0;
static const uint32_t ADDR_PIO_SET = 0x4;
static const uint32_t ADDR_PIO_CLR = 0x8;
static const uint32_t ADDR_PIO_IRQ = 0xC;

volatile uint32_t counter = 0;

static void IOirqhandler(void * context) {
	IOWR_32DIRECT(PARALLEL_PORT_0_BASE, ADDR_PIO_CLR, 0x3); // clearing bit0
}

uint32_t tooglingIO() {
	printf("Starting Set, Clr, Set, Clr Bit 0!\n");
	  while (1) {
		  IOWR_32DIRECT(PARALLEL_PORT_0_BASE, ADDR_PIO_VAL, 0x1);
		  IOWR_32DIRECT(PARALLEL_PORT_0_BASE, ADDR_PIO_VAL, 0x0);
		  IOWR_32DIRECT(PARALLEL_PORT_0_BASE, ADDR_PIO_VAL, 0x1);
		  IOWR_32DIRECT(PARALLEL_PORT_0_BASE, ADDR_PIO_VAL, 0x0);
	  }

}

uint32_t tooglingIOIRQ() {
	alt_ic_isr_register(PARALLEL_PORT_0_IRQ_INTERRUPT_CONTROLLER_ID, PARALLEL_PORT_0_IRQ, IOirqhandler, NULL, NULL);
	printf("Enabling interrupts at Bit 0, and set bit 0!\n");
	IOWR_32DIRECT(PARALLEL_PORT_0_BASE, ADDR_PIO_IRQ, 0x3);
	  while (1) {
		  IOWR_32DIRECT(PARALLEL_PORT_0_BASE, ADDR_PIO_SET, 0x3);
	  }

}

static void responseCustomTimerirqhandler(void * context) {
	IOWR_32DIRECT(CUSTOM_TIMER_0_BASE, ADDR_CNT_ENA, 0x0);
	IOWR_32DIRECT(CUSTOM_TIMER_0_BASE, ADDR_IRQ_VAL, 0x0);
}

uint32_t responseCustomTimer() {
	volatile uint32_t counter = 0;
    alt_ic_isr_register(CUSTOM_TIMER_0_IRQ_INTERRUPT_CONTROLLER_ID, CUSTOM_TIMER_0_IRQ, responseCustomTimerirqhandler, NULL, NULL);
	IOWR_32DIRECT(CUSTOM_TIMER_0_BASE, ADDR_CNT_VAL, 0xF1194D80); // (2**32-5s/20ns) in hexadecimal
	IOWR_32DIRECT(CUSTOM_TIMER_0_BASE, ADDR_IRQ_ENA, 0x1); // Enabling interrupts
	IOWR_32DIRECT(CUSTOM_TIMER_0_BASE, ADDR_CNT_ENA, 0x1); // starting counter
	while(1) {
			counter = IORD_32DIRECT(CUSTOM_TIMER_0_BASE, ADDR_CNT_VAL);
	}
}

static void recoveryCustomTimerirqhandler(void * context) {
	IOWR_ALTERA_AVALON_TIMER_CONTROL(TIMER_0_BASE,0); //Clear interrupt (ITO)
	IOWR_ALTERA_AVALON_TIMER_STATUS(TIMER_0_BASE, 0); //CLEAR TO
	IOWR_32DIRECT(CUSTOM_TIMER_0_BASE, ADDR_CNT_ENA, 0x0); // stops custom timer
	IOWR_32DIRECT(CUSTOM_TIMER_0_BASE, ADDR_IRQ_ENA, 0x0); // disables IRQ custom timer
	IOWR_32DIRECT(CUSTOM_TIMER_0_BASE, ADDR_CNT_VAL, 0x0); // sets counter to 0 custom timer
	IOWR_32DIRECT(CUSTOM_TIMER_0_BASE, ADDR_CNT_ENA, 0x1); // starts custom timer
}

uint32_t recoveryCustomTimer() {
	volatile uint32_t counter = 0;
	// using altera timer to trigger IRQ and custom timer to measure recovery time
    alt_ic_isr_register(TIMER_0_IRQ_INTERRUPT_CONTROLLER_ID, TIMER_0_IRQ, recoveryCustomTimerirqhandler, NULL, NULL);
	while(1) {
		IOWR_32DIRECT(CUSTOM_TIMER_0_BASE, ADDR_CNT_ENA, 0x0); // stopping custom counter
		counter = IORD_32DIRECT(CUSTOM_TIMER_0_BASE, ADDR_CNT_VAL);
	}
}



uint32_t main()
{
// main methods, activate only one at a time
tooglingIO();
//tooglingIOIRQ();
//responseCustomTimer();
//recoveryCustomTimer();

  return 0;
}

