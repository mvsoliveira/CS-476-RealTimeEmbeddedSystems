----------------------------------------------------------------------------------------------------------------------
-- Title      : parallel_port
-- Project    : Real time embedded systems

----------------------------------------------------------------------------------------------------------------------
-- File       : paralell_port.vhd
-- Author     : Marcos Oliveira
-- Company    : CERN
-- Created    : 2017-03-06
-- Last update: 2017-04-05
-- Platform   : Quartus II 16.1
-- Standard   : VHDL'93/02
----------------------------------------------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2016 CERN
----------------------------------------------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2017-03-06  1.0      msilvaol        Created
----------------------------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity parallel_port is
  generic (N : integer := 32);
  port (
    -- Avalon interfaces signals
    Clk        : in  std_logic;
    nReset     : in  std_logic;
    Address    : in  std_logic_vector (1 downto 0);
    ChipSelect : in  std_logic;
    Read_en    : in  std_logic;
    Write_en   : in  std_logic;
    ReadData   : out std_logic_vector (31 downto 0);
    WriteData  : in  std_logic_vector (31 downto 0);
    IRQ        : out std_logic;
    -- Parallel Port external interface
    ParPort    : out std_logic_vector (N-1 downto 0)
    );
end parallel_port;

architecture rtl of parallel_port is

  signal iReg      : std_logic_vector (N-1 downto 0);
  signal iIRQEnReg : std_logic_vector (N-1 downto 0);

begin
  main : process (Clk)
    variable IRQvar : std_logic;
  begin
    if rising_edge(Clk) then
      if nReset = '0' then
        iReg      <= (others => '0');
        iIRQEnReg <= (others => '0');
        IRQvar    := '0';
        IRQ       <= '0';
      else
        -- avalon write
        if ChipSelect = '1' and Write_en = '1' then  -- Write cycle
          case Address(1 downto 0) is
            when "00"   => iReg      <= WriteData(N-1 downto 0);  --write
            when "01"   => iReg      <= iReg or WriteData(N-1 downto 0);  --set
            when "10"   => iReg      <= iReg and not WriteData(N-1 downto 0);  --clear
            when "11"   => iIRQEnReg <= WriteData(N-1 downto 0);  --IRQ enable                                              
            when others => null;
          end case;
        end if;
        -- avalon read
        ReadData <= (others => '0');
        if (ChipSelect = '1' and Read_en = '1') then              -- Read cycle
          case Address(1 downto 0) is
            when "00" | "01" | "10" => ReadData(N-1 downto 0) <= iReg;
            when "11"               => ReadData(N-1 downto 0) <= iIRQEnReg;
            when others             => null;
          end case;
        end if;
        IRQvar := '0';
        for i in iReg'range loop
          IRQvar := IRQvar or (iReg(i) and iIRQEnReg(i));
        end loop;
        IRQ <= IRQvar;
      end if;
    end if;
  end process main;

  ParPort <= iReg;
end architecture rtl;
