-------------------------------------------------------------------------------
-- Title      : Testbench for design "timer"
-- Project    : 
-------------------------------------------------------------------------------
-- File       : timer_tb.vhd
-- Author     :   <msilvaol@PCPHL1CT05>
-- Company    : 
-- Created    : 2017-04-10
-- Last update: 2017-04-10
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2017 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2017-04-10  1.0      msilvaol        Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

-------------------------------------------------------------------------------

entity timer_tb is

end entity timer_tb;

-------------------------------------------------------------------------------

architecture tb of timer_tb is

  -- component ports
  signal Clk        : std_logic := '1';
  signal nReset     : std_logic;
  signal Address    : std_logic_vector (31 downto 0);
  signal ChipSelect : std_logic;
  signal Read_en    : std_logic;
  signal Write_en   : std_logic;
  signal ReadData   : std_logic_vector (31 downto 0);
  signal WriteData  : std_logic_vector (31 downto 0);
  signal IRQ        : std_logic;

begin  -- architecture tb

  -- component instantiation
  DUT : entity work.timer
    port map (
      Clk        => Clk,
      nReset     => nReset,
      Address    => Address(1 downto 0),
      ChipSelect => ChipSelect,
      Read_en    => Read_en,
      Write_en   => Write_en,
      ReadData   => ReadData,
      WriteData  => WriteData,
      IRQ        => IRQ);

  -- clock generation
  Clk <= not Clk after 10 ns;

  -- waveform generation
  WaveGen_Proc : process

    procedure av_clr is
    begin
      wait until Clk = '1';
      Address    <= (others => '0');
      ChipSelect <= '0';
      Read_en    <= '0';
      Write_en   <= '0';
      WriteData  <= (others => '0');
    end av_clr;

    procedure av_read (
      addr : std_logic_vector(31 downto 0)
      ) is
    begin
      wait until Clk = '1';
      Address    <= addr;
      ChipSelect <= '1';
      Read_en    <= '1';
      Write_en   <= '0';
      WriteData  <= (others => '0');
    end av_read;

    procedure av_write (
      addr : std_logic_vector(31 downto 0);
      data : std_logic_vector(31 downto 0)
      ) is
    begin
      wait until Clk = '1';
      Address    <= addr;
      ChipSelect <= '1';
      Read_en    <= '0';
      Write_en   <= '1';
      WriteData  <= data;
    end av_write;

  begin
    nReset <= '0';
    wait for 25 ns;
    nReset <= '1';
    -- clear avalon
    av_clr;
    -- Start counter
    av_write(X"00000001", X"00000001");
    av_clr;
    wait for 1 us;
    -- Stopping counter
    av_write(X"00000001", X"00000000");
    av_clr;
    wait for 100 ns;
    -- resetting counter
    av_write(X"00000000", X"FFFFFFFD");
    -- enabling interrupts
    av_write(X"00000003", X"00000001");
    -- Start counter
    av_write(X"00000001", X"00000001");
    av_clr;
    wait for 500 ns;
    -- Read counter
    av_read(X"00000000");
    -- Acknowledge interrupt
    av_write(X"00000002", X"00000000");    
    av_clr;
    wait;
  end process WaveGen_Proc;



end architecture tb;

-------------------------------------------------------------------------------

configuration timer_tb_tb_cfg of timer_tb is
  for tb
  end for;
end timer_tb_tb_cfg;

-------------------------------------------------------------------------------
