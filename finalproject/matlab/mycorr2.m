function [ r, rfl] = mycorr2( A,B )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

A = double(A);
B = double(B);
mean2A = sum(sum(A))/numel(A);
mean2Afl = round(sum(sum(A))/numel(A));
mean2B = sum(sum(B))/numel(B);
mean2Bfl = round(sum(sum(B))/numel(B));
Y = 0;
Yfl = 0;
X = 0;
Xfl = 0;
Z = 0;
Zfl = 0;


if size(A) == size(B)
    disp(' dimensions are ok')
    [il,jl] = size(A);
    for i=1:il
        for j=1:jl
            Y = Y + (A(i,j)-mean2A)*(B(i,j)-mean2B);
            Yfl = Yfl + (A(i,j)-mean2Afl)*(B(i,j)-mean2Bfl);
            X = X + (A(i,j)-mean2A)^2;
            Xfl = Xfl + (A(i,j)-mean2Afl)^2;
            Z = Z + (B(i,j)-mean2B)^2;
            Zfl = Zfl + (B(i,j)-mean2Bfl)^2;
        end
    end
    r = Y/sqrt(X*Z);
    rfl = Yfl/sqrt(Xfl*Zfl);
else
    disp(' dimensions are not ok')
end

