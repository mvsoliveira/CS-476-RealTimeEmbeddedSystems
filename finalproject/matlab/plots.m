% Plots 32Kpixel 8 k words images

N_T = [1 2 4 8 16 32 64 128 256 512 1024];
t = [2.157 1.416 1.042 0.8523 0.7592 0.7128 0.6895 0.6773 0.6714 0.6682 0.6662];


figure, plot(log2(N_T), t)                               %# plot on log2 x-scale
set(gca, 'XTickLabel',[])                      %# suppress current x-labels

xt = get(gca, 'XTick');
yl = get(gca, 'YLim');
str = cellstr( num2str(xt(:),'2^{%d}') );      %# format x-ticks as 2^{xx}
hTxt = text(xt, yl(ones(size(xt))), str, ...   %# create text at same locations
    'Interpreter','tex', ...                   %# specify tex interpreter
    'VerticalAlignment','top', ...             %# v-align to be underneath
    'HorizontalAlignment','center');           %# h-aligh to be centered

title('Time for transfering 32K pixels = 8K 32-bit words')
ylabel('Time (ms)')
xlabel('Burst Count')


n_words = [1 2 4 8 16 32 64 128 256 512 1024 2048 4096 8192 16384];

gain = [
2.8278008299
3.6295546559
5.0041067762
8.1153846154
13.997983871
25.5526315789
47.7797270955
85.1734317343
158.3512110727
291.8
488.1813261164
695.4347399411
931.1772400262
1135.2988505747
1277.3093304062
]

figure, plot(log2(n_words), log10(gain))                               %# plot on log2 x-scale
set(gca, 'XTickLabel',[])                      %# suppress current x-labels

xt = get(gca, 'XTick');
yl = get(gca, 'YLim');
str = cellstr( num2str(xt(:),'2^{%d}') );      %# format x-ticks as 2^{xx}
hTxt = text(xt, yl(ones(size(xt))), str, ...   %# create text at same locations
    'Interpreter','tex', ...                   %# specify tex interpreter
    'VerticalAlignment','top', ...             %# v-align to be underneath
    'HorizontalAlignment','center');           %# h-aligh to be centered

title('Performace gain by using the corr2 unit instead of using software only')
ylabel('log10(gain)')
xlabel('Number of 32-bit words (number of pixels/4)')

