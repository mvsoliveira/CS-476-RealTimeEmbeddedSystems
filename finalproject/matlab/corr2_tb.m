clc
n_b = 16384;
n_t = 1;
d_l = n_b*n_t;
n_p = d_l*4;
n_i = 6;
X = [];

for img = 0:n_i-1
    fprintf('Image: %d\n',img);
    if img == 0
        A = repmat([0, 128+img, 255-img, 255],1,d_l);
        A0 = A(1)+A(2)*2^8+A(3)*2^16+A(4)*2^24;
        fprintf('First Pixel Image %2d = 0x%08X\n',img,A0);
        mA = floor(sum(A)/n_p);
        fprintf('mA = %X\n',mA);
        Xtotal = (A-mA).^2;
        for p = 0:3
            X(p+1) = sum(Xtotal(p+1:4:n_p));
            fprintf('X(%d)(%d) = 0x%08X\n',img,p,X(p+1));
        end
        disp('\n')
    end
    if img > 0
       B = repmat([0, 128+img, 255-img, 255],1,d_l);
       B0 = B(1)+B(2)*2^8+B(3)*2^16+B(4)*2^24;
        fprintf('First Pixel Image %2d = 0x%08X\n',img,B0);
       mB = floor(sum(B)/n_p);
       fprintf('mB = %X\n',mB);
        Ztotal = (B-mB).^2;
        for p = 0:3
            Z(p+1) = sum(Ztotal(p+1:4:n_p));
            fprintf('Z(%d)(%d) = 0x%08X\n',img,p,Z(p+1));
        end 
        disp('\n')
        Ytotal = (A-mA).*(B-mB);
        for p = 0:3
            Y(p+1) = sum(Ytotal(p+1:4:n_p));
            fprintf('Y(%d)(%d) = 0x%08X\n',img,p,Y(p+1));
        end
        
       
    end
end
    






