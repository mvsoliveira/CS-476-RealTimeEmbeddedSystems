
% Noise
noise = 0.02;

figure, subplot(2,1,1)
I = imread('eight.tif');
J = imnoise(I,'salt & pepper',noise);
imshowpair(I,J,'montage')
fprintf('Coin corr2 = %f.\n',corr2(I,J));

%figure,
subplot(2,1,2)
I = imread('eight.tif');
J = imnoise(flipud(fliplr(I)),'salt & pepper',noise);
imshowpair(I,J,'montage')
fprintf('Coin corr2 = %f.\n',corr2(I,J));

% Comparing high and low resolution pics
figure,
A = imread('snowflakes.png');
B = imresize(A, 0.25, 'nearest');
B = imresize(B, size(A), 'nearest');
subplot(2,2,1), imshow(A)
subplot(2,2,2), imshow(B)
fprintf('Planet corr2 = %f.\n',corr2(A,B));

A = imread('snowflakes.png');
B = imresize(A, 0.25, 'nearest');
B = flipud(fliplr(imresize(B, size(A), 'nearest')));
subplot(2,2,3), imshow(A)
subplot(2,2,4), imshow(B)
fprintf('Planet corr2 = %f.\n',corr2(A,B));





