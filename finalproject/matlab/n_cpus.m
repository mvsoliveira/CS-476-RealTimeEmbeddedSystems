t_compfull = 74e-6; % time to compute r=Y/sqrt(X*Z)
t_irq = 3.6e-6; % response + recovery time
t_4byte = 50e-9; % minimum
t_compmean = 0.000002214;
n_max_pixel = 1e4;

pixels = [0:4:n_max_pixel];
t_dma = t_irq + t_compmean + ceil(pixels/4)*t_4byte;

n_proccpu = ceil(t_compfull./t_dma);

figure, plot(pixels/1000,n_proccpu)
title('Number of CPUs needed to compute the 2D correlation with maximum performance')
ylabel('Number of auxiliar CPUs to achieve highest performance')
xlabel('Number of pixels in multiples of 10e3')







