library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity calb is

  generic (
    width_in : natural := 9);           -- INput data width

  port (
    clk   : in  std_logic;                      -- clock
    sload : in  std_logic;                      -- clear accumulator
    ena   : in  std_logic;                      -- enable 
    Amn   : in  signed(width_in-1 downto 0);    -- Amn
    mA    : in  signed(width_in-1 downto 0);    -- mean of A
    Bmn   : in  signed(width_in-1 downto 0);    -- Bmn
    mB    : in  signed(width_in-1 downto 0);    -- mean of B
    Y     : out signed(31 downto 0);  -- Y
    XZ    : out signed(31 downto 0)   -- XZ    
    );

end entity calb;

architecture rtl of calb is

  signal AmnmA : signed (width_in-1 downto 0);
  signal BmnmB : signed (width_in-1 downto 0);

  component signed_adder_subtractor is
    generic (
      DATA_WIDTH : natural);
    port (
      a       : in  signed ((DATA_WIDTH-1) downto 0);
      b       : in  signed ((DATA_WIDTH-1) downto 0);
      add_sub : in  std_logic;
      ena     : in  std_logic;
      result  : out signed ((DATA_WIDTH-1) downto 0));
  end component signed_adder_subtractor;

  component signed_multiply_accumulate is
    generic (
      DATA_WIDTH : natural);
    port (
      a         : in  signed((DATA_WIDTH-1) downto 0);
      b         : in  signed ((DATA_WIDTH-1) downto 0);
      clk       : in  std_logic;
      sload     : in  std_logic;
      accum_out : out signed (31 downto 0));
  end component signed_multiply_accumulate;

begin  -- architecture rtl

  signed_adder_subtractor_1 : signed_adder_subtractor
    generic map (
      DATA_WIDTH => width_in)
    port map (
      a       => Amn,
      b       => mA,
      add_sub => '0',
      ena => ena,
      result  => AmnmA);

    signed_adder_subtractor_2 : signed_adder_subtractor
    generic map (
      DATA_WIDTH => width_in)
    port map (
      a       => Bmn,
      b       => mB,
      add_sub => '0',
      ena => ena,
      result  => BmnmB);

  signed_multiply_accumulate_1 : signed_multiply_accumulate
    generic map (
      DATA_WIDTH => width_in)
    port map (
      a         => AmnmA,
      b         => BmnmB,
      clk       => clk,
      sload     => sload,
      accum_out => Y);

  signed_multiply_accumulate_2 : signed_multiply_accumulate
    generic map (
      DATA_WIDTH => width_in)
    port map (
      a         => BmnmB,
      b         => BmnmB,
      clk       => clk,
      sload     => sload,
      accum_out => XZ);


end architecture rtl;
