	component SoC is
		port (
			clk_clk                             : in    std_logic                     := 'X';             -- clk
			new_sdram_controller_0_wire_addr    : out   std_logic_vector(12 downto 0);                    -- addr
			new_sdram_controller_0_wire_ba      : out   std_logic_vector(1 downto 0);                     -- ba
			new_sdram_controller_0_wire_cas_n   : out   std_logic;                                        -- cas_n
			new_sdram_controller_0_wire_cke     : out   std_logic;                                        -- cke
			new_sdram_controller_0_wire_cs_n    : out   std_logic;                                        -- cs_n
			new_sdram_controller_0_wire_dq      : inout std_logic_vector(15 downto 0) := (others => 'X'); -- dq
			new_sdram_controller_0_wire_dqm     : out   std_logic_vector(1 downto 0);                     -- dqm
			new_sdram_controller_0_wire_ras_n   : out   std_logic;                                        -- ras_n
			new_sdram_controller_0_wire_we_n    : out   std_logic;                                        -- we_n
			pll_0_outclk1_clk                   : out   std_logic;                                        -- clk
			reset_reset_n                       : in    std_logic                     := 'X';             -- reset_n
			uart_0_external_connection_rxd      : in    std_logic                     := 'X';             -- rxd
			uart_0_external_connection_txd      : out   std_logic;                                        -- txd
			corr2_0_conduit_end_calbena_conduit : out   std_logic;                                        -- calbena_conduit
			corr2_0_conduit_end_dmafire_conduit : out   std_logic;                                        -- dmafire_conduit
			corr2_0_conduit_end_dmairq_conduit  : out   std_logic;                                        -- dmairq_conduit
			corr2_0_conduit_end_irqack_conduit  : out   std_logic;                                        -- irqack_conduit
			pll_0_locked_export                 : out   std_logic                                         -- export
		);
	end component SoC;

	u0 : component SoC
		port map (
			clk_clk                             => CONNECTED_TO_clk_clk,                             --                         clk.clk
			new_sdram_controller_0_wire_addr    => CONNECTED_TO_new_sdram_controller_0_wire_addr,    -- new_sdram_controller_0_wire.addr
			new_sdram_controller_0_wire_ba      => CONNECTED_TO_new_sdram_controller_0_wire_ba,      --                            .ba
			new_sdram_controller_0_wire_cas_n   => CONNECTED_TO_new_sdram_controller_0_wire_cas_n,   --                            .cas_n
			new_sdram_controller_0_wire_cke     => CONNECTED_TO_new_sdram_controller_0_wire_cke,     --                            .cke
			new_sdram_controller_0_wire_cs_n    => CONNECTED_TO_new_sdram_controller_0_wire_cs_n,    --                            .cs_n
			new_sdram_controller_0_wire_dq      => CONNECTED_TO_new_sdram_controller_0_wire_dq,      --                            .dq
			new_sdram_controller_0_wire_dqm     => CONNECTED_TO_new_sdram_controller_0_wire_dqm,     --                            .dqm
			new_sdram_controller_0_wire_ras_n   => CONNECTED_TO_new_sdram_controller_0_wire_ras_n,   --                            .ras_n
			new_sdram_controller_0_wire_we_n    => CONNECTED_TO_new_sdram_controller_0_wire_we_n,    --                            .we_n
			pll_0_outclk1_clk                   => CONNECTED_TO_pll_0_outclk1_clk,                   --               pll_0_outclk1.clk
			reset_reset_n                       => CONNECTED_TO_reset_reset_n,                       --                       reset.reset_n
			uart_0_external_connection_rxd      => CONNECTED_TO_uart_0_external_connection_rxd,      --  uart_0_external_connection.rxd
			uart_0_external_connection_txd      => CONNECTED_TO_uart_0_external_connection_txd,      --                            .txd
			corr2_0_conduit_end_calbena_conduit => CONNECTED_TO_corr2_0_conduit_end_calbena_conduit, --         corr2_0_conduit_end.calbena_conduit
			corr2_0_conduit_end_dmafire_conduit => CONNECTED_TO_corr2_0_conduit_end_dmafire_conduit, --                            .dmafire_conduit
			corr2_0_conduit_end_dmairq_conduit  => CONNECTED_TO_corr2_0_conduit_end_dmairq_conduit,  --                            .dmairq_conduit
			corr2_0_conduit_end_irqack_conduit  => CONNECTED_TO_corr2_0_conduit_end_irqack_conduit,  --                            .irqack_conduit
			pll_0_locked_export                 => CONNECTED_TO_pll_0_locked_export                  --                pll_0_locked.export
		);

