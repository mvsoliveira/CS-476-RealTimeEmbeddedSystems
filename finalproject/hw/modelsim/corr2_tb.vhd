----------------------------------------------------------------------------------------------------------------------
-- Title      : Testbench for design "corr2"
-- Project    : MUCTPI

----------------------------------------------------------------------------------------------------------------------
-- File       : corr2_tb.vhd
-- Author     : Marcos Oliveira
-- Company    : CERN
-- Created    : 2017-06-11
-- Last update: 2017-06-11
-- Platform   : Symplify Premier G-2012.09 and Mentor Modelsim SE-64 10.1c
-- Standard   : VHDL'93/02
----------------------------------------------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2017 CERN
----------------------------------------------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2017-06-11  1.0      msilvaol        Created
----------------------------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


------------------------------------------------------------------------------------------------------------------------

entity corr2_tb is

end entity corr2_tb;

------------------------------------------------------------------------------------------------------------------------

architecture tb of corr2_tb is

  constant n_b : natural := 16;       -- number of bursts
  constant n_t : natural := 1024;          -- number of transfers per burst (burst count)
  constant d_l : natural := n_b*n_t;    -- total number of words
  constant n_p : natural := d_l*4;      -- total numbe rof pixels
  constant n_i : natural := 6;          -- number of images
  constant f_l : natural := 16384;      -- fifo length


  constant d_l_v : std_logic_vector(31 downto 0) := std_logic_vector(to_unsigned(d_l, 32));


  -- component ports
  signal nReset         : std_logic;
  signal AS_Address     : std_logic_vector (31 downto 0);
  signal AS_ChipSelect  : std_logic;
  signal AS_IRQ         : std_logic;
  signal AS_Read        : std_logic;
  signal AS_Write       : std_logic;
  signal AS_ReadData    : std_logic_vector (31 downto 0);
  signal AS_WriteData   : std_logic_vector (31 downto 0);
  signal AM_Address     : std_logic_vector (31 downto 0);
  signal AM_ByteEnable  : std_logic_vector(3 downto 0);
  signal AM_BurstCount  : std_logic_vector(10 downto 0);
  signal AM_Read        : std_logic;
  signal AM_WaitRequest : std_logic;
  signal AM_ReadValid   : std_logic;
  signal AM_ReadData    : std_logic_vector (31 downto 0);
  signal IRQAck         : std_logic;
  signal CALBEna        : std_logic;
  signal DMAFire        : std_logic;
  signal DMAIrq         : std_logic;

  -- clock
  signal Clk : std_logic := '1';
  -- reporting
  --signal readdata : std_logic_vector(31 downto 0);

begin  -- architecture tb

  -- component instantiation
  DUT : entity work.corr2
    port map (
      Clk            => Clk,
      nReset         => nReset,
      AS_Address     => AS_Address(7 downto 0),
      AS_ChipSelect  => AS_ChipSelect,
      AS_IRQ         => AS_IRQ,
      AS_Read        => AS_Read,
      AS_Write       => AS_Write,
      AS_ReadData    => AS_ReadData,
      AS_WriteData   => AS_WriteData,
      AM_Address     => AM_Address,
      AM_ByteEnable  => AM_ByteEnable,
      AM_BurstCount  => AM_BurstCount,
      AM_Read        => AM_Read,
      AM_WaitRequest => AM_WaitRequest,
      AM_ReadValid   => AM_ReadValid,
      AM_ReadData    => AM_ReadData,
      IRQAck         => IRQAck,
      CALBEna        => CALBEna,
      DMAFire        => DMAFire,
      DMAIrq         => DMAIrq);

  -- clock generation
  Clk <= not Clk after 10 ns;

  -- waveform generation
  WaveGen_Proc : process
    variable readdata : std_logic_vector(31 downto 0);

    function to_string (a : std_logic_vector) return string is
      variable b    : string (1 to a'length) := (others => NUL);
      variable stri : integer                := 1;
    begin
      for i in a'range loop
        b(stri) := std_logic'image(a((i)))(2);
        stri    := stri+1;
      end loop;
      return b;
    end function;

    function to_hstring (value : std_logic_vector) return string is
      constant ne     : integer := (value'length+3)/4;
      variable pad    : std_logic_vector(0 to (ne*4 - value'length) - 1);
      variable ivalue : std_logic_vector(0 to ne*4 - 1);
      variable result : string(1 to ne);
      variable quad   : std_logic_vector(0 to 3);
    begin
      if value'length < 1 then
        return "NUS";
      else
        if value (value'left) = 'Z' then
          pad := (others => 'Z');
        else
          pad := (others => '0');
        end if;
        ivalue := pad & value;
        for i in 0 to ne-1 loop
          quad := To_X01Z(ivalue(4*i to 4*i+3));
          case quad is
            when x"0"   => result(i+1) := '0';
            when x"1"   => result(i+1) := '1';
            when x"2"   => result(i+1) := '2';
            when x"3"   => result(i+1) := '3';
            when x"4"   => result(i+1) := '4';
            when x"5"   => result(i+1) := '5';
            when x"6"   => result(i+1) := '6';
            when x"7"   => result(i+1) := '7';
            when x"8"   => result(i+1) := '8';
            when x"9"   => result(i+1) := '9';
            when x"A"   => result(i+1) := 'A';
            when x"B"   => result(i+1) := 'B';
            when x"C"   => result(i+1) := 'C';
            when x"D"   => result(i+1) := 'D';
            when x"E"   => result(i+1) := 'E';
            when x"F"   => result(i+1) := 'F';
            when "ZZZZ" => result(i+1) := 'Z';
            when others => result(i+1) := 'X';
          end case;
        end loop;
        return result;
      end if;
    end function to_hstring;

    function to_stdv (value : integer; size : integer) return std_logic_vector is
    begin
      return std_logic_vector(to_unsigned(value, size));
    end function to_stdv;

    procedure avs_clr is
    begin
      wait until Clk = '1';
      AS_Address    <= (others => '0');
      AS_ChipSelect <= '0';
      AS_Read       <= '0';
      AS_Write      <= '0';
      AS_WriteData  <= (others => '0');
    end avs_clr;

    procedure avs_read (
      addr          :     std_logic_vector(31 downto 0);
      variable data : out std_logic_vector(31 downto 0)
      ) is
    begin
      wait until Clk = '1';
      AS_Address    <= addr;
      AS_ChipSelect <= '1';
      AS_Read       <= '1';
      AS_Write      <= '0';
      AS_WriteData  <= (others => '0');
      wait until Clk = '1';
      wait for 1 fs;
      data          := AS_ReadData;
    end avs_read;

    procedure avs_write (
      addr : std_logic_vector(31 downto 0);
      data : std_logic_vector(31 downto 0)
      ) is
    begin
      wait until Clk = '1';
      AS_Address    <= addr;
      AS_ChipSelect <= '1';
      AS_Read       <= '0';
      AS_Write      <= '1';
      AS_WriteData  <= data;
    end avs_write;

    procedure avm_clr is
    begin
      wait until Clk = '1';
      AM_WaitRequest <= '1';
      AM_ReadValid   <= '0';
      AM_ReadData    <= (others => '0');
    end avm_clr;

    procedure avm_ack is
    begin
      wait until Clk = '1';
      AM_WaitRequest <= '0';
      AM_ReadValid   <= '0';
      AM_ReadData    <= (others => '0');
    end avm_ack;

    procedure avm_read (
      data : std_logic_vector(31 downto 0)
      ) is
    begin
      wait until Clk = '1';
      AM_WaitRequest <= '1';
      AM_ReadValid   <= '1';
      AM_ReadData    <= data;
    end avm_read;

    procedure avs_read_print (
      addr    : std_logic_vector(31 downto 0);
      message : string

      ) is
      variable readdata : std_logic_vector(31 downto 0);
    begin
      avs_read(addr, readdata);
      report message & to_hstring(readdata);
    end avs_read_print;

    procedure avs_write_mean (img : integer)is
      variable readdata : std_logic_vector(31 downto 0);
      variable acc      : integer;
      variable mean     : integer;
      variable mean_v   : std_logic_vector(31 downto 0);
    begin
      avs_read(X"00000004", readdata);
      acc    := to_integer(unsigned(readdata));
      mean   := acc/n_p;
      mean_v := to_stdv(mean, 32);
      if img = 0 then
        avs_write(X"0000000E", mean_v);
        avs_write(X"0000000F", mean_v);
      else
        avs_write(X"0000000F", mean_v);
      end if;
      report "Accumulator Value: 0x" & to_hstring(readdata) & " | Mean Value: 0x" & to_hstring(mean_v);
    end procedure avs_write_mean;

    variable varPixel0 : integer;
    variable varPixel1 : integer;
    variable varPixel2 : integer;
    variable varPixel3 : integer;
    variable varInt    : integer;
    variable varStd    : std_logic_vector(31 downto 0);

    procedure send_image(img : integer) is
      variable varPixel0 : std_logic_vector(7 downto 0);
      variable varPixel1 : std_logic_vector(7 downto 0);
      variable varPixel2 : std_logic_vector(7 downto 0);
      variable varPixel3 : std_logic_vector(7 downto 0);
      variable varWord   : std_logic_vector(31 downto 0);
    begin
      for b in 0 to n_b-1 loop
        wait until AM_Read = '1';
        wait for 100 ns;
        avm_ack;
        avm_clr;
        for i in 1 to n_t loop

          varPixel0 := std_logic_vector(to_unsigned(0, 8));
          varPixel1 := std_logic_vector(to_unsigned(128+img, 8));
          varPixel2 := std_logic_vector(to_unsigned(255-img, 8));
          varPixel3 := std_logic_vector(to_unsigned(255, 8));

          varWord := varPixel3 & varPixel2 & varPixel1 & varPixel0;

          avm_read(varWord);
          avm_clr;
        end loop;
      end loop;
    end procedure send_image;

  begin
    -- clear avalon
    avs_clr;
    avm_clr;
    -- reset
    nReset <= '0';
    wait for 25 ns;
    nReset <= '1';

    --------------------------------------------------------------------------------------------------------------------
    -- Initialization
    --------------------------------------------------------------------------------------------------------------------

    report "========== Initialization ==========";
    -- reading Firmware version
    avs_read_print(X"00000000", "Firmware Version: 0x");

    -- Make sure IRQ masking and force IRQ is disabled
    avs_write(X"00000005", X"00000000");

    -- Setting fixed DMA registers (burst count, and FIFO threshold)
    avs_write(X"0000000A", to_stdv(n_t, 32));
    avs_write(X"0000000B", to_stdv(f_l-n_t, 32));
    avs_read_print(X"0000000A", "DMA Burst Count: 0x");
    avs_read_print(X"0000000B", "DMA Fifo Threshold: 0x");

    -- set CALB data length 
    avs_write(X"0000000D", to_stdv(d_l, 32));

    --------------------------------------------------------------------------------------------------------------------
    -- Processing per image
    --------------------------------------------------------------------------------------------------------------------

    for img in 0 to n_i-1 loop
      report "========== Image: " & integer'image(img) & " ==========";

      -- writing, reading, and printing DMA addresses
      avs_write(X"00000008", to_stdv(img*d_l, 32));
      avs_write(X"00000009", to_stdv((img+1)*d_l-n_t, 32));

      avs_read_print(X"00000008", "DMA Start Addr: 0x");
      avs_read_print(X"00000009", "DMA End Addr: 0x");


      -- making sure Fifo / Calb settings register is properly configured
      if img = 0 then
        -- FIFO mode = 0, flusing FIFOs
        avs_write(X"00000001", X"00000006");
        avs_write(X"00000001", X"00000000");
      elsif img = 1 then
        -- FIFO mode = 1
        avs_write(X"00000001", X"00000001");
      else
        -- FIFO and CALB mode = 1
        avs_write(X"00000001", X"00000009");
      end if;
      avs_read_print(X"00000001", "Fifo / Calb register: 0x");
      avs_read_print(X"00000002", "FIFOA Used words before DMA: 0x");
      avs_read_print(X"00000003", "FIFOB Used words before DMA: 0x");

      if img = 0 then
        -- DMA Fire, CALB not enabled (loading A)
        avs_write(X"00000007", X"00000001");
      else
        avs_write(X"00000007", X"00000005");
      end if;
      avs_clr;

      -- Loading 32*4=128 pixels in 2 bursts of 16 transfers
      report "Sending image: " & integer'image(img) & ".";
      send_image(img);
      report "Finished sending image: " & integer'image(img) & ".";

      wait until AS_IRQ = '1';
      wait for 100 ns;
      -- acknowlege IRQ
      avs_write(X"00000006", X"00000000");

      -- Checking Accumulator result
      -- Sn = (a1+an)*n/2
      -- Acc = 4*(1+32)*32/2 = 2112 = 0x840
      avs_write_mean(img);

      -- checking again number of words in the FIFO
      avs_read_print(X"00000002", "FIFOA Used words after DMA: 0x");
      avs_read_print(X"00000003", "FIFOB Used words after DMA: 0x");

      if img > 0 then
        -- Reading X=Sum_m(Sum_n([Amn-mean2(A)]^2))
        avs_read_print(X"00000014", "XZ[" & integer'image(img-1) & "][0]= 0x");
        avs_read_print(X"00000015", "XZ[" & integer'image(img-1) & "][1]= 0x");
        avs_read_print(X"00000016", "XZ[" & integer'image(img-1) & "][2]= 0x");
        avs_read_print(X"00000017", "XZ[" & integer'image(img-1) & "][3]= 0x");
      end if;

      if img > 1 then
        -- Reading X=Sum_m(Sum_n([Amn-mean2(A)]^2))
        avs_read_print(X"00000010", "Y[" & integer'image(img-1) & "][0]= 0x");
        avs_read_print(X"00000011", "Y[" & integer'image(img-1) & "][1]= 0x");
        avs_read_print(X"00000012", "Y[" & integer'image(img-1) & "][2]= 0x");
        avs_read_print(X"00000013", "Y[" & integer'image(img-1) & "][3]= 0x");
      end if;

      avs_clr;

    end loop;  -- loop img
    wait;

  end process WaveGen_Proc;




end architecture tb;

