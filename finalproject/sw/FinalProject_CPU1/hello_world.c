#include <stdio.h>
#include <stdint.h>
#include "system.h"
#include "io.h"
#include "sys/alt_timestamp.h"
#include "altera_avalon_mailbox_simple.h"
#include "math.h"

uint32_t tstamp_start_gl = 0;

#define CPU_N 0x01

static altera_avalon_mailbox_dev* m_box;

#define IMG_START NEW_SDRAM_CONTROLLER_0_BASE

typedef struct {
	uint32_t img_size;
	uint8_t img_amount;
} MBStruct;

uint32_t AC_software(MBStruct mb_obj) {
	uint32_t A_sum = 0;
	for (uint32_t ptr = 0; ptr < mb_obj.img_size; ++ptr) {
		A_sum += IORD_8DIRECT(IMG_START, ptr);
	}
	//printf("A_sum =%d\n", A_sum);
	//long double A_mean = A_sum/(long double)mb_obj.img_size;
	double A_mean = A_sum/(double)mb_obj.img_size;
	printf("A_mean_SW =%f\n", A_mean);
	double X = 0;
	for (uint32_t ptr = 0; ptr < mb_obj.img_size; ++ptr) {
		X += ((IORD_8DIRECT(IMG_START, ptr) - A_mean)*(IORD_8DIRECT(IMG_START, ptr) - A_mean));
	}
	//printf("X = %d\n", X);
	X = sqrt(X);
	for (uint8_t img = 1; img < mb_obj.img_amount; ++img) {
		uint32_t end = (img + 1)*mb_obj.img_size;
		uint32_t B_sum = 0;
		//printf("data= %d", IORD_8DIRECT(IMG_START, img*mb_obj.img_size));
		//printf("data= %d", IORD_8DIRECT(IMG_START, end - 1));
		//printf("data= %d", img*mb_obj.img_size);
		for (uint32_t ptr = img*mb_obj.img_size; ptr < end; ++ptr) {
			B_sum += IORD_8DIRECT(IMG_START, ptr);
		}
		//printf("B_mean_SUM = %d", B_sum);
		//long double B_mean = B_sum/(long double)mb_obj.img_size;
		double B_mean = B_sum/(double)mb_obj.img_size;
		//printf("B_mean_SW = %d\n", B_mean);
		double Y = 0;
		double Z = 0;
		uint32_t A_ptr = 0;
		for (uint32_t ptr = img*mb_obj.img_size; ptr < end; ++ptr) {
			Y += (IORD_8DIRECT(IMG_START, A_ptr) - A_mean)*(IORD_8DIRECT(IMG_START, ptr) - B_mean);
			Z += (IORD_8DIRECT(IMG_START, ptr) - B_mean)*(IORD_8DIRECT(IMG_START, ptr) - B_mean);
		    A_ptr++;
		}
		//printf("Y = %d", Y);
		//printf("Z = %d", Z);
		long double result = Y/(X*sqrt(Z));
		printf("RES: the coefficient for the image #%d = %f\n", img, result);
	}
	return 0;
}

#define ACC_F_VERSION 0x00
#define ACC_F_VERSION_VAL 0xABCD0001
#define DMA_START 0x08*4
#define DMA_END 0x09*4
#define BRUST_COUNT 0x0A*4
#define BURTS_COUNT_VAL 8
#define FIFO_THR  0x0B*4
#define FIFO_CALB_CFG 0x01*4
#define DMA_FIRE 0x07*4
#define IRQ_MASK 0x05*4
#define IRQ_ACK 0x06*4
#define ACC_ACC 0x04*4
#define CALB_LEN 0x0D*4
#define A_MEAN_BACK 0x0E*4
#define B_MEAN_BACK 0x0F*4

#define X_DATA_0 0x14*4
#define X_DATA_1 0x15*4
#define X_DATA_2 0x16*4
#define X_DATA_3 0x17*4

#define Y_DATA_0 0x10*4
#define Y_DATA_1 0x11*4
#define Y_DATA_2 0x12*4
#define Y_DATA_3 0x13*4

typedef enum {LOAD_A, MEAN_A, LOAD_B, FINISH, END} accSt;

// Accelerator's SW state machine
static accSt ac_state = LOAD_A;
static uint32_t img_size_ = 0;
static uint32_t img_amount_ = 0;
static uint32_t img_counter = 0;
static uint32_t X = 0;
static uint8_t LOAD_B_VERSION = 0;

// Accelerator's IRQ
void AC_ISR (void* context, alt_u32 id) {
	// Ack. the interrupt
	IOWR_32DIRECT(CORR2_0_BASE, IRQ_ACK, 0x00);
	// Check the current state
	uint32_t acc = 0;
	uint32_t tstamp_stop = 0;
	uint32_t ts_frq = 0;
	switch (ac_state) {
	case MEAN_A:
		// The the acc. value
		acc = IORD_32DIRECT(CORR2_0_BASE, ACC_ACC);
		//printf("deb_2= %x\n", IORD_32DIRECT(CORR2_0_BASE, 0x02*4));
		//printf("deb_3= %x\n", IORD_32DIRECT(CORR2_0_BASE, 0x03*4));
		//double A_mean = acc/(double)img_size_;
		uint32_t A_mean = acc/img_size_;
		// TODO: rounding
		//printf("A_mean_HW= %d\n", A_mean);
		//printf("A_mean_HW_rounded= %d\n", A_mean);
		// Write it back
		IOWR_32DIRECT(CORR2_0_BASE, A_MEAN_BACK, (uint32_t)A_mean);
		IOWR_32DIRECT(CORR2_0_BASE, B_MEAN_BACK, (uint32_t)A_mean);
		//IOWR_32DIRECT(CORR2_0_BASE, A_MEAN_BACK, 0);
		img_counter++;
		//printf("DEB: img_counter= %d\n", img_counter);
		// Setting the fifo mode to 1
		IOWR_32DIRECT(CORR2_0_BASE, FIFO_CALB_CFG, 0x01);
		// Set the next DMA credentials
		IOWR_32DIRECT(CORR2_0_BASE, DMA_START, img_size_/4);
		IOWR_32DIRECT(CORR2_0_BASE, DMA_END, 2*img_size_/4 - BURTS_COUNT_VAL);
		// DMA fire
		IOWR_32DIRECT(CORR2_0_BASE, DMA_FIRE, 0x05);
		ac_state = LOAD_B;
		break;
	case LOAD_B:
		//printf("deb_2= %x\n", IORD_32DIRECT(CORR2_0_BASE, 0x02*4));
		//printf("deb_3= %x\n", IORD_32DIRECT(CORR2_0_BASE, 0x03*4));
		// The acc. calue
		acc = IORD_32DIRECT(CORR2_0_BASE, ACC_ACC);
		//double B_mean = acc/(double)img_size_;
		uint32_t B_mean = acc/img_size_;
		//printf("B_mean_HW= %d\n", B_mean);
		// Write it back. TODO: rounding
		IOWR_32DIRECT(CORR2_0_BASE, B_MEAN_BACK, (uint32_t)B_mean);
		img_counter++;
		//printf("DEB: img_counter= %d\n", img_counter);
		// Read X
		if (LOAD_B_VERSION == 0) {
			X = IORD_32DIRECT(CORR2_0_BASE, X_DATA_0) + IORD_32DIRECT(CORR2_0_BASE, X_DATA_1) +
					IORD_32DIRECT(CORR2_0_BASE, X_DATA_2) + IORD_32DIRECT(CORR2_0_BASE, X_DATA_3);
			/*printf("X_HW = %d\n", IORD_32DIRECT(CORR2_0_BASE, X_DATA_0));
			printf("X_HW = %d\n", IORD_32DIRECT(CORR2_0_BASE, X_DATA_1));
			printf("X_HW = %d\n", IORD_32DIRECT(CORR2_0_BASE, X_DATA_2));
			printf("X_HW = %d\n", IORD_32DIRECT(CORR2_0_BASE, X_DATA_3));*/
			//printf("X_HW = %d\n", X);
			LOAD_B_VERSION = 1;
		} else {
			uint32_t Z = IORD_32DIRECT(CORR2_0_BASE, X_DATA_0) + IORD_32DIRECT(CORR2_0_BASE, X_DATA_1) +
					IORD_32DIRECT(CORR2_0_BASE, X_DATA_2) + IORD_32DIRECT(CORR2_0_BASE, X_DATA_3);
			int32_t Y = IORD_32DIRECT(CORR2_0_BASE, Y_DATA_0) + IORD_32DIRECT(CORR2_0_BASE, Y_DATA_1) +
					IORD_32DIRECT(CORR2_0_BASE, Y_DATA_2) + IORD_32DIRECT(CORR2_0_BASE, Y_DATA_3);
			printf("DEB: X= %d\n", X);
			printf("DEB: Y= %d\n", Y);
			printf("DEB: Z= %d\n", Z);
			double res = Y/(sqrt(X)*sqrt(Z));
			printf("RES: the coefficient for the image #%d is %f\n", img_counter-2, res);
		}
		//printf("IMAGE_COUNTER = %d\n", img_counter);
		//printf("IMAGE_AMOUNT = %d\n", img_amount_);
		if (img_counter < img_amount_ + 1) {
			// Setting the fifo mode to 1
			ac_state = LOAD_B;
			IOWR_32DIRECT(CORR2_0_BASE, FIFO_CALB_CFG, 0x01);
			IOWR_32DIRECT(CORR2_0_BASE, DMA_START, img_counter*img_size_/4);
			IOWR_32DIRECT(CORR2_0_BASE, DMA_END, (img_counter + 1)*img_size_/4 - BURTS_COUNT_VAL);
			// DMA fire
			IOWR_32DIRECT(CORR2_0_BASE, DMA_FIRE, 0x05);
		} else {
			tstamp_stop = alt_timestamp();
			ts_frq = alt_timestamp_freq();
			printf("TIME: HW implementation %f sec\n", (tstamp_stop - tstamp_start_gl)/(double)ts_frq);
			ac_state = FINISH;
		}
		break;
	default:
		break;
	}
}

void AC_hardware() {
	switch (ac_state) {
	case LOAD_A:
		// Configure the DMA
		IOWR_32DIRECT(CORR2_0_BASE, DMA_START, 0x00);
		IOWR_32DIRECT(CORR2_0_BASE, DMA_END, img_size_/4 - BURTS_COUNT_VAL);
		IOWR_32DIRECT(CORR2_0_BASE, BRUST_COUNT, BURTS_COUNT_VAL);
		IOWR_32DIRECT(CORR2_0_BASE, FIFO_THR, 0x3FFF);
		// Clear the FIFO's
		IOWR_32DIRECT(CORR2_0_BASE, FIFO_CALB_CFG, 0x06);
		IOWR_32DIRECT(CORR2_0_BASE, FIFO_CALB_CFG, 0x00);
		// test
		//printf("deb_1= %x\n", IORD_32DIRECT(CORR2_0_BASE, 0x01*4));
		//printf("deb_2= %x\n", IORD_32DIRECT(CORR2_0_BASE, 0x02*4));
		//printf("deb_3= %x\n", IORD_32DIRECT(CORR2_0_BASE, 0x03*4));
		// DMA fire
		IOWR_32DIRECT(CORR2_0_BASE, DMA_FIRE, 0x01);
		ac_state = MEAN_A;
		break;
	default:
		break;
	}
}

void doJob() {
	while(1) {
		MBStruct mb_obj;
		altera_avalon_mailbox_retrieve_poll(m_box, &mb_obj, 0);
		printf("INFO: Recieved a task. The images size is: %d; amount = %d\n", mb_obj.img_size, mb_obj.img_amount);
		uint32_t ts_frq = alt_timestamp_freq();
		alt_timestamp_start();
		uint32_t tstamp_start = alt_timestamp();
		AC_software(mb_obj);
		uint32_t tstamp_stop = alt_timestamp();
		printf("TIME: SW implementation %f sec\n", (tstamp_stop - tstamp_start)/(double)ts_frq);

		img_size_ = mb_obj.img_size;
		img_amount_ = mb_obj.img_amount;
		init();

		tstamp_start_gl = alt_timestamp();
		AC_hardware();

	}
}

void init1() {
	m_box = altera_avalon_mailbox_open(MAILBOX_SIMPLE_0_NAME, NULL, NULL);
	if (m_box == NULL) goto err;
	printf("INFO: init is done\n");
	return;
	err:
	printf("ERROR: failed to init the system\n");
}

// Init everything
uint8_t context_val = 0;
void init() {
	// Init the accelerator
	if (IORD_32DIRECT(CORR2_0_BASE, ACC_F_VERSION) != ACC_F_VERSION_VAL) {
		printf("ERROR: The accelerator isn't found\n");
		goto err;
	}
	// Make sure IRQ masking and force IRQ is disabled
	IOWR_32DIRECT(CORR2_0_BASE, IRQ_MASK, 0x00);
	// Set CALB length
	IOWR_32DIRECT(CORR2_0_BASE, CALB_LEN, img_size_/4);
	// Init the interrupt
	alt_irq_register(CORR2_0_IRQ, &context_val, AC_ISR);
	return;
	err:
	printf("ERROR: failed to init the system\n");
}

int main()
{
	printf("CPU %d started\n", CPU_N);
	init1();
	doJob();
	/*mb.img_amount = 4;
	mb.img_size = 26400;
	AC_software(mb);
	AC_hardware();
	while(1);*/
	return 0;
}
