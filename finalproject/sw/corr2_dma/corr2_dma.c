/*
 * Corr2 DMA controller.
 */

#include <stdio.h>
#include <inttypes.h>
#include "io.h"
#include "system.h"
#include "sys/alt_irq.h"

#define DEBUG_LOW
//#define DEBUG_LOW_IMG
#define FMT "%-20s = 0x%08X.\n"
#define FMT_V "%2s[%02d][%02d] %9s = 0x%08X.\n"

#define N_B  255// number of bursts
#define N_T  64 // number of transfers per burst (burst count)
#define D_L  N_B*N_T // total number of words
#define N_P  D_L*4 // total number of pixels
#define N_I  6 // number of images
#define S_A  0x0 // start address for images

static const uint32_t FIFO_THRSHLD = 0x3FFF;

static const uint32_t ADDR_FW_VERSION = 0x0;
static const uint32_t ADDR_FIFOCALB_CFG = 0x4;
static const uint32_t ADDR_FIFO_USEDWA = 0x8;
static const uint32_t ADDR_FIFO_USEDWB = 0xC;
static const uint32_t ADDR_ACC_RES = 0x10;
static const uint32_t ADDR_IRQ_SET = 0x14;
static const uint32_t ADDR_IRQ_ACK = 0x18;
static const uint32_t ADDR_FIFOCALB_RUN = 0x1C;
static const uint32_t ADDR_DMA_START = 0x20;
static const uint32_t ADDR_DMA_END = 0x24;
static const uint32_t ADDR_BURST_CNT = 0x28;
static const uint32_t ADDR_FIFO_THRSHLD = 0x2C;
static const uint32_t ADDR_CALB_DLENGTH = 0x34;
static const uint32_t ADDR_CALB_MA = 0x38;
static const uint32_t ADDR_CALB_MB = 0x3C;

static const uint32_t ADDR_XZ0 = 0x50;
static const uint32_t ADDR_XZ1 = 0x54;
static const uint32_t ADDR_XZ2 = 0x58;
static const uint32_t ADDR_XZ3 = 0x5C;

static const uint32_t ADDR_Y0 = 0x40;
static const uint32_t ADDR_Y1 = 0x44;
static const uint32_t ADDR_Y2 = 0x48;
static const uint32_t ADDR_Y3 = 0x4C;

static const uint32_t DIV = N_P;

volatile uint32_t IMG_I = 0x0;
volatile uint32_t IRQ_D = 0x1;

volatile uint32_t XZ0;
volatile uint32_t XZ1;
volatile uint32_t XZ2;
volatile uint32_t XZ3;
volatile uint32_t Y0;
volatile uint32_t Y1;
volatile uint32_t Y2;
volatile uint32_t Y3;

volatile uint32_t ACC;
volatile uint32_t MEAN;
volatile uint32_t FIFO_USEDWA;
volatile uint32_t FIFO_USEDWB;
volatile uint32_t CALB_MA;
volatile uint32_t CALB_MB;


static void irqhandler(void * context) {
	IRQ_D = 0x1;
	//Acknowledge IRQ
	IOWR_32DIRECT(CORR2_0_BASE, ADDR_IRQ_ACK,0x0);
	ACC = IORD_32DIRECT(CORR2_0_BASE, ADDR_ACC_RES);
	MEAN = ACC/((uint32_t)N_P);
	if (IMG_I == 0x0) {
		IOWR_32DIRECT(CORR2_0_BASE, ADDR_CALB_MA, MEAN);
		IOWR_32DIRECT(CORR2_0_BASE, ADDR_CALB_MB, MEAN);
	} else {
		IOWR_32DIRECT(CORR2_0_BASE, ADDR_CALB_MB, MEAN);
	}
	if (IMG_I > 0x0) {
		XZ0 = IORD_32DIRECT(CORR2_0_BASE, ADDR_XZ0);
		XZ1 = IORD_32DIRECT(CORR2_0_BASE, ADDR_XZ1);
		XZ2 = IORD_32DIRECT(CORR2_0_BASE, ADDR_XZ2);
		XZ3 = IORD_32DIRECT(CORR2_0_BASE, ADDR_XZ3);
	}
	if (IMG_I > 0x1) {
		Y0 = IORD_32DIRECT(CORR2_0_BASE, ADDR_Y0);
		Y1 = IORD_32DIRECT(CORR2_0_BASE, ADDR_Y1);
		Y2 = IORD_32DIRECT(CORR2_0_BASE, ADDR_Y2);
		Y3 = IORD_32DIRECT(CORR2_0_BASE, ADDR_Y3);
	}

#ifdef DEBUG_LOW
	FIFO_USEDWA = IORD_32DIRECT(CORR2_0_BASE, ADDR_FIFO_USEDWA);
	FIFO_USEDWB = IORD_32DIRECT(CORR2_0_BASE, ADDR_FIFO_USEDWB);
	CALB_MA = IORD_32DIRECT(CORR2_0_BASE, ADDR_CALB_MA);
	CALB_MB = IORD_32DIRECT(CORR2_0_BASE, ADDR_CALB_MB);
#endif
	IMG_I++;
}

void print_irq_res(){
#ifdef DEBUG_LOW
	if (IMG_I > 0x0) {
		printf("========== Printing results IMG = %02d. ========== \n", IMG_I-1);
		printf(FMT, "FIFO_USEDWA", FIFO_USEDWA);
		printf(FMT, "FIFO_USEDWB", FIFO_USEDWA);
		printf(FMT, "ACC", ACC);
		printf(FMT, "MEAN", MEAN);
		printf(FMT, "CALB_MA", CALB_MA);
		printf(FMT, "CALB_MB", CALB_MB);
	}
	if (IMG_I > 0x1) {
		printf(FMT_V, "XZ", IMG_I-2, 0, "", XZ0);
		printf(FMT_V, "XZ", IMG_I-2, 1, "", XZ1);
		printf(FMT_V, "XZ", IMG_I-2, 2, "", XZ2);
		printf(FMT_V, "XZ", IMG_I-2, 3, "", XZ3);
	}
	if (IMG_I > 0x2) {
		printf(FMT_V, "Y", IMG_I-3, 0, "", Y0);
		printf(FMT_V, "Y", IMG_I-3, 1, "", Y1);
		printf(FMT_V, "Y", IMG_I-3, 2, "", Y2);
		printf(FMT_V, "Y", IMG_I-3, 3, "", Y3);
	}
#endif

}

void read_print(uint32_t ADDR, char MSG[])
{
	uint32_t readdata = IORD_32DIRECT(CORR2_0_BASE, ADDR);
	printf(FMT, MSG, readdata);
}

void read_print_sdram(uint32_t img, uint32_t p)
{
	uint32_t readdata = IORD_32DIRECT(NEW_SDRAM_CONTROLLER_0_BASE, p);
	printf("Image %02d - Pointer 0x%08X - Content: 0x%08X.\n", img, p, readdata);
}

int init()
{
	//Make sure IRQ masking and force IRQ is disabled
	IOWR_32DIRECT(CORR2_0_BASE, ADDR_IRQ_SET,0x0);
	volatile uint32_t irq;
	irq = alt_irq_register(CORR2_0_IRQ, NULL, (void*) irqhandler);
	// Setting fixed DMA registers (burst count, and FIFO threshold)
	IOWR_32DIRECT(CORR2_0_BASE, ADDR_BURST_CNT,N_T);
	IOWR_32DIRECT(CORR2_0_BASE, ADDR_FIFO_THRSHLD,FIFO_THRSHLD);
	// Setting CALB data length
	IOWR_32DIRECT(CORR2_0_BASE, ADDR_CALB_DLENGTH,D_L);
#ifdef DEBUG_LOW
	printf("Initialisation\n");
	read_print(ADDR_FW_VERSION, "FW VERSION");
	read_print(ADDR_IRQ_SET, "IRQ_SET");
	read_print(ADDR_BURST_CNT, "BURST_CNT");
	read_print(ADDR_FIFO_THRSHLD, "FIFO_THRSHLD");
	read_print(ADDR_CALB_DLENGTH, "CALB_DLENGTH");
#endif

	return 0;
}

void loadimages()
{
	uint32_t varPixel0;
	uint32_t varPixel1;
	uint32_t varPixel2;
	uint32_t varPixel3;
	uint32_t varWord;

	uint32_t p = S_A;

	for (uint32_t img = 0; img < N_I;img++) {
		for (uint32_t b = 0; b < N_B; b++) {
			for (uint32_t i = 1; i <= N_T; i++) {

				varPixel0 = 0;
				varPixel1 = 128+img;
				varPixel2 = 255-img;
				varPixel3 = 255;

				varWord   =  (varPixel3 << 24) + (varPixel2 << 16) + (varPixel1 << 8) + varPixel0;

				IOWR_32DIRECT(NEW_SDRAM_CONTROLLER_0_BASE,p,varWord);
				p +=4;

			}
		}
	}

#ifdef DEBUG_LOW_IMG
	printf("Showing image content \n");
	p = S_A;
	for (uint32_t img = 1; img <= N_I;img++) {
		for (uint32_t k = 0; k <= D_L-1; k++) {
			read_print_sdram(img, p);
			p +=4;
		}
	}
#endif


}

void set_acc_image() {
	IRQ_D = 0x0;
	IOWR_32DIRECT(CORR2_0_BASE, ADDR_DMA_START,IMG_I*D_L);
	IOWR_32DIRECT(CORR2_0_BASE, ADDR_DMA_END,(IMG_I+1)*D_L-N_T);
	if (IMG_I == 0x0) {
		// FIFO mode = 0, flushing FIFOs
		IOWR_32DIRECT(CORR2_0_BASE, ADDR_FIFOCALB_CFG,0x6);
		IOWR_32DIRECT(CORR2_0_BASE, ADDR_FIFOCALB_CFG,0x0);
	} else if (IMG_I == 0x1) {
		// FIFO mode = 1
		IOWR_32DIRECT(CORR2_0_BASE, ADDR_FIFOCALB_CFG,0x1);
	} else {
		// FIFO and CALB mode = 1
		IOWR_32DIRECT(CORR2_0_BASE, ADDR_FIFOCALB_CFG,0x9);
	}
#ifdef DEBUG_LOW
	printf("========== Setting IMG = %02d. ========== \n", IMG_I);
	read_print(ADDR_DMA_START, "DMA_START");
	read_print(ADDR_DMA_END, "DMA_END");
	read_print(ADDR_FIFOCALB_CFG, "FIFOCALB_CFG");
	read_print(ADDR_FIFO_USEDWA, "FIFO_USEDWA");
	read_print(ADDR_FIFO_USEDWB, "FIFO_USEDWB");
#endif
	if (IMG_I == 0x0) {
		// DMA Fire, CALB not enabled (loading A)
		IOWR_32DIRECT(CORR2_0_BASE, ADDR_FIFOCALB_RUN,0x1);
	} else {
		// DMA Fire, CALB enabled (loading others)
		IOWR_32DIRECT(CORR2_0_BASE, ADDR_FIFOCALB_RUN,0x5);
	}
}

uint32_t mean_c(uint32_t addr) {
	uint32_t acc = 0;
	uint32_t mean;
	uint32_t readdata;
	for (uint32_t p = addr; p < addr+4*D_L; p += 4) {
		readdata = IORD_32DIRECT(NEW_SDRAM_CONTROLLER_0_BASE,p);
		for (uint32_t i = 0; i < 32 ; i+=8) {
			acc += (readdata & (0xFF << i)) >> i;
		}
	}
	mean = acc/((uint32_t)N_P);
	return mean;
}

void corr2_c() { //not working yet 
	uint32_t mA;
	uint32_t mB;
	uint32_t Amn;
	uint32_t Bmn;
	uint32_t X = 0;
	int32_t Y;
	uint32_t Z;
	uint32_t readdataA;
	uint32_t readdataB;
	uint32_t pB = S_A + 4*D_L;
	int32_t subA;
	int32_t subB;

	for (uint32_t img = 0; img < N_I;img++) {
		Y = 0;
		Z = 0;
		if (img == 0) {
			mA = mean_c(S_A);
		}
		mB = mean_c(pB);
		for (uint32_t pA = S_A ; pA < S_A+4*D_L; pA+=4) {
			readdataA = IORD_32DIRECT(NEW_SDRAM_CONTROLLER_0_BASE,pA);
			readdataB = IORD_32DIRECT(NEW_SDRAM_CONTROLLER_0_BASE,pB);
			pB +=4;
			if (img == 0) {
				for (uint32_t i = 0; i < 32 ; i+=8) {
					Amn = ((readdataA & (0xFF << i)) >> i);
					subA = Amn - mA;
					X += subA*subA;
				}
			}
			for (uint32_t i = 0; i < 32 ; i+=8) {
				Amn = ((readdataA & (0xFF << i)) >> i);
				Bmn = ((readdataB & (0xFF << i)) >> i);
				subA = Amn - mA;
				subB = Bmn - mB;
				Z += subB*subB;
				Y += subA*subB;
			}
		}
	printf(FMT_V, "X", img, 0, "", X);
	printf(FMT_V, "Y", img, 0, "", Y);
	printf(FMT_V, "Z", img, 0, "", Z);
	}

}

int main()
{
	loadimages(N_I);
	corr2_c();
	init();

	while (IMG_I <= N_I) {
		if (IRQ_D == 0x1) {
			print_irq_res();
			set_acc_image();
		}

	}
	return 0;
}



