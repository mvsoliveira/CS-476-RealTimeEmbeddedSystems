#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "system.h"
#include "io.h"
#include "altera_avalon_mailbox_simple.h"

#define CPU_N 0x00

#define IMG_START NEW_SDRAM_CONTROLLER_0_BASE

static FILE* uart_0;
static altera_avalon_mailbox_dev* m_box;

uint8_t  load_img_amount() {
	return getc(uart_0);
}

uint32_t load_image(uint32_t data_ptr) {
	uint8_t c = 0;
	uint32_t size = 0;
	while(c < 4) {
		size |= (getc(uart_0) << c*0x08);
		c++;
	}
	uint32_t data_end = data_ptr + size;
	while(data_ptr < data_end) {
		IOWR_8DIRECT(IMG_START, data_ptr, getc(uart_0));
		data_ptr++;
	}
	printf("INFO: the image is loaded!\n");
	return size;
}

uint32_t check_image(uint32_t data_ptr, uint32_t size) {
	uint32_t sum = 0;
	for(uint32_t i = 0; i < size; ++i) {
		sum += IORD_8DIRECT(IMG_START, data_ptr + i);
	}
	return sum;
}

/*void check_images(uint32_t size, uint32_t amount) {
	printf("test: %d %d", size, amount);
	for (uint32_t img = 0; img < amount; ++img) {
		uint32_t end = (img + 1)*size;
		printf("test: %d %d", img, end);
		uint32_t sum = 0;
		for(uint32_t ptr = img*size; ptr < end; ++ptr) {
			sum += IORD_8DIRECT(IMG_START, ptr);
		}
		printf ("IMAGE #%d sum is %d\n", img, sum);
	}
}*/

#define NEXT_IMG 0x01
typedef enum {LOAD_AMOUNT, LOAD_IMAGES, PROCESSING} lSM;
typedef struct {
	lSM state;
	uint8_t img_amount;
	uint8_t img_counter;
	uint32_t data_ptr;
} lSM_h;

typedef struct {
	uint32_t img_size;
	uint8_t img_amount;
} MBStruct;

static MBStruct mb_data;
static lSM_h hloadSM;
void load_state_machine_call() {
	switch (hloadSM.state) {
	case LOAD_AMOUNT:
		hloadSM.img_amount = load_img_amount();
		printf("INFO: going to load %d images", hloadSM.img_amount);
		hloadSM.state = LOAD_IMAGES;
		hloadSM.img_counter = 0;
		break;
	case LOAD_IMAGES:
		mb_data.img_size = load_image(hloadSM.data_ptr);
		printf("INFO: the image #%d is loaded. The SUM= %d\n", hloadSM.img_counter, check_image(hloadSM.data_ptr, mb_data.img_size));
		fprintf(uart_0, "%d", NEXT_IMG);
		hloadSM.data_ptr += mb_data.img_size;
		hloadSM.img_counter++;
		if (hloadSM.img_counter == hloadSM.img_amount) hloadSM.state = PROCESSING;
		break;
	case PROCESSING:
		//printf("Processing...\n");
		mb_data.img_amount = hloadSM.img_amount;
		//check_images(mb_data.img_size, mb_data.img_amount);
		mb_data.img_amount = hloadSM.img_amount;
		altera_avalon_mailbox_send(m_box, &mb_data, 0, POLL);
		hloadSM.state = LOAD_AMOUNT;
		break;
	default:
		break;
	}
}

// Init everything
void init() {
	// Init the state machine
	hloadSM.state = LOAD_AMOUNT;
	hloadSM.img_amount = 0;
	hloadSM.img_counter = 0;
	hloadSM.data_ptr = 0;
	// Init the UART
	uart_0 = fopen (UART_0_NAME, "r+");
	if (uart_0 == NULL) goto err;
	// Open the mailbox
	m_box = altera_avalon_mailbox_open(MAILBOX_SIMPLE_0_NAME, NULL, NULL);
	if (m_box == NULL) goto err;
	printf("INFO: init is done!\n");
	return;
	err:
	printf("ERROR: failed to init the system\n");
}

int main()
{
  printf("CPU %d started\n", CPU_N);
  init();
  while(1) {
	  load_state_machine_call();
  }

  return 0;
}
