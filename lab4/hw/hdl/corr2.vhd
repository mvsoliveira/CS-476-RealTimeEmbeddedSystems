----------------------------------------------------------------------------------------------------------------------
-- Title      : 2D correlation
-- Project    : CS-476 Real Time Embedded Systems
----------------------------------------------------------------------------------------------------------------------
-- File       : corr2.vhd
-- Author     : Marcos Oliveira
-- Company    : CERN
-- Created    : 2017-06-09
-- Last update: 2017-06-11
-- Platform   : Symplify Premier G-2012.09 and Mentor Modelsim SE-64 10.1c
-- Standard   : VHDL'93/02
----------------------------------------------------------------------------------------------------------------------
-- Description: Corr2 top level
-------------------------------------------------------------------------------
-- Copyright (c) 2017 CERN
----------------------------------------------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2016-12-21  1.0      msilvaol        Created
----------------------------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity corr2 is

  port (
    -- Clock and reset
    Clk            : in  std_logic;
    nReset         : in  std_logic;
    -- Avalon slave interfaces signals
    AS_Address     : in  std_logic_vector (7 downto 0);
    AS_ChipSelect  : in  std_logic;
    AS_IRQ         : out std_logic;
    AS_Read        : in  std_logic;
    AS_Write       : in  std_logic;
    AS_ReadData    : out std_logic_vector (31 downto 0);
    AS_WriteData   : in  std_logic_vector (31 downto 0);
    -- Avalon Master Signals
    AM_Address     : out std_logic_vector (31 downto 0);
    AM_ByteEnable  : out std_logic_vector(3 downto 0);
    AM_BurstCount  : out std_logic_vector(10 downto 0);
    AM_Read        : out std_logic;
    AM_WaitRequest : in  std_logic;
    AM_ReadValid   : in  std_logic;
    AM_ReadData    : in  std_logic_vector (31 downto 0);
    -- monitoring
    IRQAck         : out std_logic;
    CALBEna        : out std_logic;
    DMAFire        : out std_logic;
    DMAIrq         : out std_logic
    );
end corr2;

architecture rtl of corr2 is

  --fifos
  signal fifoA_clock : std_logic;
  signal fifoA_data  : std_logic_vector (31 downto 0);
  signal fifoA_rdreq : std_logic;
  signal fifoA_sclr  : std_logic;
  signal fifoA_wrreq : std_logic;
  signal fifoA_q     : std_logic_vector (31 downto 0);
  signal fifoA_usedw : std_logic_vector (13 downto 0);
  signal fifoB_clock : std_logic;
  signal fifoB_data  : std_logic_vector (31 downto 0);
  signal fifoB_rdreq : std_logic;
  signal fifoB_sclr  : std_logic;
  signal fifoB_wrreq : std_logic;
  signal fifoB_q     : std_logic_vector (31 downto 0);
  signal fifoB_usedw : std_logic_vector (13 downto 0);
  signal fifo_usedw  : std_logic_vector (13 downto 0);


  -- avalon registers
  constant Sumlength         : natural := 8 + 2;
  constant Acclength         : natural := Sumlength + fifo_usedw'length;
  signal regAccResult        : unsigned(Acclength-1 downto 0);
  signal regCalbEna          : std_logic;
  signal regCalbMode         : std_logic;
  signal regDataLength       : std_logic_vector(fifo_usedw'range);
  signal regDmaBurstCount    : unsigned(10 downto 0);
  signal regDmaEndAddr       : unsigned(31 downto 0);
  signal regDmaFifoThreshold : unsigned(13 downto 0);
  signal regDmaFire          : std_logic;
  signal regDmaIrqAck        : std_logic;
  signal regDmaStartAddr     : unsigned(31 downto 0);
  signal regDmaSyncRst       : std_logic;
  signal regFifoAFlush       : std_logic;
  signal regFifoBFlush       : std_logic;
  signal regFifoMode         : std_logic;
  signal regMaskIRQ          : std_logic;
  signal regmA               : std_logic_vector(7 downto 0);
  signal regmB               : std_logic_vector(7 downto 0);

  -- interruption
  signal irq        : std_logic;
  -- dma
  signal dma_addr   : unsigned(31 downto 0);
  signal dma_irq    : std_logic;
  signal dma_rx_cnt : unsigned(10 downto 0);
  signal dma_rx_d   : std_logic;
  type dma_s_t is (waiting, avalon, transfering, irqreq);
  signal dma_s      : dma_s_t;

  -- CALB
  signal calb_cnt : unsigned(regDataLength'range);
  signal calb_ena : std_logic;
  signal calb_clr : std_logic;
  signal Amn      : signed(31 downto 0);
  signal Bmn      : signed(31 downto 0);
  signal mA       : signed(7 downto 0);
  signal mB       : signed(7 downto 0);
  type signed_array is array (integer range <>) of signed(15 downto 0);
  signal Y        : signed_array(0 to 3);
  signal XZ       : signed_array(0 to 3);


begin  -- architecture rtl

  ----------------------------------------------------------------------------------------------------------------------
  -- Main Logic
  ----------------------------------------------------------------------------------------------------------------------

  main : process (Clk, nReset)
    variable varPixel0   : unsigned(Sumlength-1 downto 0);
    variable varPixel1   : unsigned(Sumlength-1 downto 0);
    variable varPixel2   : unsigned(Sumlength-1 downto 0);
    variable varPixel3   : unsigned(Sumlength-1 downto 0);
    variable varSumPixel : unsigned(Sumlength-1 downto 0);

  begin
    if nReset = '0' then

      --AM_Address          <= (others => '0');
      --AM_BurstCount       <= (others => '0');
      --AM_ByteEnable       <= (others => '0');
      --AM_Read             <= '0';
      AS_ReadData         <= (others => '0');
      dma_addr            <= (others => '0');
      --dma_irq             <= '0';
      dma_rx_cnt          <= (others => '0');
      dma_rx_d            <= '0';
      dma_s               <= waiting;
      irq                 <= '0';
      regAccResult        <= (others => '0');
      regCalbEna          <= '0';
      regCalbMode         <= '0';
      regDataLength       <= (others => '0');
      regDmaBurstCount    <= (others => '0');
      regDmaEndAddr       <= (others => '0');
      regDmaFifoThreshold <= (others => '0');
      regDmaFire          <= '0';
      regDmaIrqAck        <= '0';
      regDmaStartAddr     <= (others => '0');
      regDmaSyncRst       <= '0';
      regFifoAFlush       <= '0';
      regFifoBFlush       <= '0';
      regFifoMode         <= '0';
      regMaskIRQ          <= '0';
      regmA               <= (others => '0');
      regmB               <= (others => '0');

    elsif rising_edge(Clk) then
      -------------------------------------------------------------------------
      -- Write Avalon Logic
      -------------------------------------------------------------------------
      -- self clear values
      regDmaIrqAck  <= '0';
      regDmaFire    <= '0';
      regDmaSyncRst <= '0';
      if AS_ChipSelect = '1' and AS_Write = '1' then  -- Write cycle
        case AS_Address(7 downto 0) is
          when x"00" =>
            null;
          when x"01" =>
            regFifoMode   <= AS_WriteData(0);
            regFifoAFlush <= AS_WriteData(1);
            regFifoBFlush <= AS_WriteData(2);
            regCalbMode   <= AS_WriteData(3);
          when x"02" =>
            null;
          when x"03" =>
            null;
          when x"04" =>
            null;
          when x"05" =>
            regMaskIRQ <= AS_WriteData(1);
            irq        <= AS_WriteData(0);
          when x"06" =>
            regDmaIrqAck <= '1';
          when x"07" =>
            regCalbEna    <= AS_WriteData(2);
            regDmaSyncRst <= AS_WriteData(1);
            regDmaFire    <= AS_WriteData(0);
          when x"08" =>
            regDmaStartAddr <= unsigned(AS_WriteData);
          when x"09" =>
            regDmaEndAddr <= unsigned(AS_WriteData);
          when x"0A" =>
            regDmaBurstCount <= unsigned(AS_WriteData(10 downto 0));
          when x"0B" =>
            regDmaFifoThreshold <= unsigned(AS_WriteData(13 downto 0));
          when x"0C" =>
            null;
          when x"0D" =>
            regDataLength <= AS_WriteData(regDataLength'range);
          when x"0E" =>
            regmA <= AS_WriteData(regmA'range);
          when x"0F" =>
            regmB <= AS_WriteData(regmB'range);
          when others => null;
        end case;
      end if;

      -------------------------------------------------------------------------
      -- Read Avalon Logic
      -------------------------------------------------------------------------  
      AS_ReadData <= (others => '0');
      if (AS_ChipSelect = '1' and AS_Read = '1') then  -- Read cycle
        -- default values
        case AS_Address(7 downto 0) is
          when x"00" =>                                -- FW version
            AS_ReadData <= X"abcd0001";
          when x"01" =>
            AS_ReadData(0) <= regFifoMode;
            AS_ReadData(1) <= regFifoAFlush;
            AS_ReadData(2) <= regFifoBFlush;
            AS_ReadData(3) <= regCalbMode;
          when x"02" =>
            AS_ReadData(fifoA_usedw'range) <= fifoA_usedw;
          when x"03" =>
            AS_ReadData(fifoB_usedw'range) <= fifoB_usedw;
          when x"04" =>
            AS_ReadData(regAccResult'range) <= std_logic_vector(regAccResult);
          when x"05" =>
            AS_ReadData(1) <= regMaskIRQ;
            AS_ReadData(0) <= irq;
          when x"06" =>
            null;
          when x"07" =>
            AS_ReadData(3) <= calb_ena;
            AS_ReadData(2) <= regCalbEna;
          when x"08" =>
            AS_ReadData(regDmaStartAddr'range) <= std_logic_vector(regDmaStartAddr);
          when x"09" =>
            AS_ReadData(regDmaEndAddr'range) <= std_logic_vector(regDmaEndAddr);
          when x"0A" =>
            AS_ReadData(regDmaBurstCount'range) <= std_logic_vector(regDmaBurstCount);
          when x"0B" =>
            AS_ReadData(regDmaFifoThreshold'range) <= std_logic_vector(regDmaFifoThreshold);
          when x"0C" =>
            null;
          when x"0D" =>
            null;
          when x"0E" =>
            AS_ReadData(regmA'range) <= regmA;
          when x"0F" =>
            AS_ReadData(regmB'range) <= regmB;
          when x"10" =>
            AS_ReadData(Y(0)'range) <= std_logic_vector(Y(0));
          when x"11" =>
            AS_ReadData(Y(1)'range) <= std_logic_vector(Y(1));
          when x"12" =>
            AS_ReadData(Y(2)'range) <= std_logic_vector(Y(2));
          when x"13" =>
            AS_ReadData(Y(3)'range) <= std_logic_vector(Y(3));
          when x"14" =>
            AS_ReadData(XZ(0)'range) <= std_logic_vector(XZ(0));
          when x"15" =>
            AS_ReadData(XZ(1)'range) <= std_logic_vector(XZ(1));
          when x"16" =>
            AS_ReadData(XZ(2)'range) <= std_logic_vector(XZ(2));
          when x"17" =>
            AS_ReadData(XZ(3)'range) <= std_logic_vector(XZ(3));
          when x"18" =>
            AS_ReadData <= std_logic_vector(dma_addr);
          when x"19" =>
            AS_ReadData(10 downto 0) <= std_logic_vector(dma_rx_cnt);
          when x"1A" =>
            AS_ReadData(10 downto 0) <= std_logic_vector(regDmaBurstCount);
          when x"1B" =>
            AS_ReadData(regDmaFifoThreshold'range) <= std_logic_vector(regDmaFifoThreshold);
          when x"1C" =>
            AS_ReadData(1 downto 0) <= std_logic_vector(to_unsigned(dma_s_t'pos(dma_s), 2));
          when others => null;
        end case;
      end if;

      -------------------------------------------------------------------------
      -- DMA
      -------------------------------------------------------------------------
      -- default values
      --AM_Address    <= (others => '0');
      --AM_ByteEnable <= (others => '0');
      --AM_BurstCount <= (others => '0');
      --AM_Read       <= '0';
      --dma_irq       <= '0';
      case dma_s is
        when waiting =>                                         -- waiting for firing the whole DMA frame transfer
          if regDmaFire = '1' then
            dma_s    <= avalon;
            dma_addr <= regDmaStartAddr;
          end if;
        when avalon =>                                          -- issuing DMA burst transfer
          --AM_Address    <= std_logic_vector(dma_addr);
          --AM_ByteEnable <= (others => '1');
          --AM_BurstCount <= std_logic_vector(regDmaBurstCount);
          --AM_Read       <= '1';
          if AM_WaitRequest = '0' then                          -- the command was successfully sent?
            dma_s      <= transfering;
            dma_rx_cnt <= (others => '0');
          end if;
        when transfering =>
          if dma_rx_cnt >= regDmaBurstCount then                -- transfer received
            if unsigned(fifo_usedw) < regDmaFifoThreshold then  -- there is space?
              if dma_addr >= regDmaEndAddr then                 -- is the whole frame finished?
                dma_s <= irqreq;
              else                                              -- frame not finished, new transfer then
                dma_s    <= avalon;
                dma_addr <= dma_addr + regDmaBurstCount;
              end if;
            end if;
          elsif dma_rx_d = '1' then                             -- if transfer not complete and data comes
            dma_rx_cnt <= dma_rx_cnt + 1;
          end if;
        when irqreq =>                                          -- issuing interruption request
          --dma_irq <= '1';
          if regDmaIrqAck = '1' then                            -- is the interruption acknowleged?
            dma_s <= waiting;
          end if;
        when others => null;
      end case;
      -- delaying cnt value for aligning with fifousedw
      dma_rx_d <= AM_ReadValid;
      -- sync reset DMA
      if regDmaSyncRst = '1' then
        dma_s <= waiting;
      end if;

      ------------------------------------------------------------------------------------------------------------------
      -- Acumulator
      ------------------------------------------------------------------------------------------------------------------
      varPixel0   := unsigned("00" & AM_ReadData(31 downto 24));
      varPixel1   := unsigned("00" & AM_ReadData(23 downto 16));
      varPixel2   := unsigned("00" & AM_ReadData(15 downto 8));
      varPixel3   := unsigned("00" & AM_ReadData(7 downto 0));
      varSumPixel := (varPixel0 + varPixel1) + (varPixel2 + varPixel3);

      if regDmaFire = '1' then
        regAccResult <= (others => '0');
      elsif AM_ReadValid = '1' then
        regAccResult <= regAccResult + varSumPixel;
      end if;

    end if;
  end process main;

-- combinatorial process for the DMA FSM
  dma_fsm_p : process (dma_s)
  begin

    AM_Address    <= (others => '0');
    AM_ByteEnable <= (others => '0');
    AM_BurstCount <= (others => '0');
    AM_Read       <= '0';
    dma_irq       <= '0';
    case dma_s is
      when waiting => null;
      when avalon =>
        AM_Address    <= std_logic_vector(dma_addr);
        AM_ByteEnable <= (others => '1');
        AM_BurstCount <= std_logic_vector(regDmaBurstCount);
        AM_Read       <= '1';
      when transfering => null;
      when irqreq =>
        dma_irq <= '1';
      when others => null;
    end case;

  end process dma_fsm_p;


  AS_IRQ  <= (irq or dma_irq) and not regMaskIRQ;
  -- logic analyser timing monitoring signals 
  IRQAck  <= regDmaIrqAck;
  CALBEna <= calb_ena;
  DMAFire <= regDmaFire;
  DMAIrq  <= dma_irq;


  ----------------------------------------------------------------------------------------------------------------------
  -- FIFOs
  ----------------------------------------------------------------------------------------------------------------------

  fifoA_clock <= Clk;
  fifoA_data  <= AM_ReadData  when regFifoMode = '0' else fifoA_q;
  fifoA_rdreq <= calb_ena;
  fifoA_sclr  <= regFifoAFlush;
  fifoA_wrreq <= AM_ReadValid when regFifoMode = '0' else fifoA_rdreq;

  fifoB_clock <= Clk;
  fifoB_data  <= AM_ReadData;
  fifoB_rdreq <= '0'          when regCalbMode = '0' else calb_ena;
  fifoB_sclr  <= regFifoBFlush;
  fifoB_wrreq <= AM_ReadValid when regFifoMode = '1' else '0';

  fifo_usedw <= fifoA_usedw when regFifoMode = '0' else fifoB_usedw;

  fifo_A : entity work.fifo
    port map (
      clock => fifoA_clock,
      data  => fifoA_data,
      rdreq => fifoA_rdreq,
      sclr  => fifoA_sclr,
      wrreq => fifoA_wrreq,
      q     => fifoA_q,
      usedw => fifoA_usedw);

  fifo_B : entity work.fifo
    port map (
      clock => fifoB_clock,
      data  => fifoB_data,
      rdreq => fifoB_rdreq,
      sclr  => fifoB_sclr,
      wrreq => fifoB_wrreq,
      q     => fifoB_q,
      usedw => fifoB_usedw);


  ----------------------------------------------------------------------------------------------------------------------
  -- Custom Arithmetic Logic Blocks
  ----------------------------------------------------------------------------------------------------------------------


  Amn <= signed(fifoA_q);
  Bmn <= signed(fifoA_q) when regCalbMode = '0' else signed(fifoB_q);
  mA  <= signed(regmA);
  mB  <= signed(regmB);

  calb_p : process (Clk, nReset)
  begin
    if nReset = '0' then
      calb_ena <= '0';
      calb_cnt <= (others => '0');
      calb_clr <= '0';

    elsif rising_edge(Clk) then
      if regDmaFire = '1' then          -- priority encoding
        calb_cnt <= (others => '0');
        calb_clr <= '1';
      else
        calb_clr <= '0';
        if regCalbEna = '1' then
          if calb_cnt <= unsigned(regDataLength) then
            calb_cnt <= calb_cnt +1;
            calb_ena <= '1';
          else
            calb_ena <= '0';
          end if;
        end if;
      end if;
    end if;
  end process calb_p;


  GEN_CALB : for I in 0 to 3 generate

    calb : entity work.calb
      generic map (
        width_in => 8)
      port map (
        clk   => clk,
        sload => calb_clr,
        ena   => calb_ena,
        Amn   => Amn((I+1)*8-1 downto I*8),
        mA    => mA,
        Bmn   => Bmn((I+1)*8-1 downto I*8),
        mB    => mB,
        Y     => Y(I),
        XZ    => XZ(I));

  end generate GEN_CALB;

end architecture rtl;
