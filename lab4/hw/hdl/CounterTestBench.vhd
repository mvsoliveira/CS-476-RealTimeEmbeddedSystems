LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.ALL;

ENTITY test_tb IS 
END test_tb;

ARCHITECTURE behavior OF test_tb IS

	COMPONENT Counter
	PORT(
	 -- Avalon interface signals
		 Clk        : IN std_logic;
		 nReset     : IN std_logic;

	 -- Avalon Slave interfaces signals
		 S_Address    : IN std_logic_vector (1 DOWNTO 0);
		 S_ChipSelect : IN std_logic;

		 S_Read       : IN std_logic;
		 S_Write      : IN std_logic;

		 S_ReadData   : OUT std_logic_vector (31 DOWNTO 0);
		 S_WriteData  : IN std_logic_vector (31 DOWNTO 0);
		 
	 -- Interrupt line
		 IRQ			: OUT std_logic
	 );
    END COMPONENT;

   --declare inputs and initialize them
   signal clk : std_logic := '0';
   constant clk_period : time := 20 ns;

	signal s_addr : std_logic_vector (1 DOWNTO 0);
	signal S_Wr   : std_logic;
	signal S_WData  : std_logic_vector (31 DOWNTO 0);
	signal S_Rd   : std_logic;
	signal S_RData  : std_logic_vector (31 DOWNTO 0);
	
	signal irq_l    : std_logic;

BEGIN
    -- Instantiate the Unit Under Test (UUT)
   uut: Counter PORT MAP (
          Clk => clk,
			 nReset => '1',
			 S_Address =>  s_addr,
			 S_ChipSelect => '1',
			 S_Read => S_Rd,
			 S_Write => S_Wr,
			 S_WriteData => S_WData,
			 S_ReadData => S_RData,
			 IRQ => irq_l
        );       

   -- Clock process definitions
   clk_process :process
   begin
        clk <= '0';
        wait for clk_period/2;  --for 0.5 ns signal is '0'.
        clk <= '1';
        wait for clk_period/2;  --for next 0.5 ns signal is '1'.
   end process;
	
	stim_proc: process
   begin         
        wait for 20 ms;
		  --------------
		  -- Test control sequence and reading the value
		  --------------
		  -- Start the counter, wait a bit and read the value
        s_addr <= "00";
		  S_WData <= std_logic_vector(to_unsigned(16#01#,S_WData'length));
		  S_Wr <= '1';					-- write
        wait for 15 ns;
        S_Wr <= '0';
		  wait for 15 ns;
		  s_addr <= "10";
		  S_Rd <= '1';					-- Read
		  wait for 50 ns;
        S_Rd <= '0';
		  
		  -- Reset
		  s_addr <= "00";
		  S_WData <= std_logic_vector(to_unsigned(16#03#,S_WData'length));
		  S_Wr <= '1';					-- write
        wait for 15 ns;
        S_Wr <= '0';
		  wait for 35 ns;
		  s_addr <= "10";
		  S_Rd <= '1';					-- Read
		  wait for 15 ns;
        S_Rd <= '0';

        wait;
  end process;

END;
