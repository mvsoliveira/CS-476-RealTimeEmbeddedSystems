----------------------------------------------------------------------------------------------------------------------
-- Title      : Custom Arithmetic Logic Block
-- Project    : CS-476 Real Time Embedded Systems

----------------------------------------------------------------------------------------------------------------------
-- File       : calb.vhd
-- Author     : Marcos Oliveira
-- Company    : CERN
-- Created    : 2017-06-10
-- Last update: 2017-06-10
-- Platform   : Symplify Premier G-2012.09 and Mentor Modelsim SE-64 10.1c
-- Standard   : VHDL'93/02
----------------------------------------------------------------------------------------------------------------------
-- Description: Performs Subtraction, Multiplication and acumulation for 2D correlation
-------------------------------------------------------------------------------
-- Copyright (c) 2017 CERN
----------------------------------------------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2017-06-10  1.0      msilvaol        Created
----------------------------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity calb is

  generic (
    width_in : natural := 8);           -- INput data width

  port (
    clk   : in  std_logic;                      -- clock
    sload : in  std_logic;                      -- clear accumulator
    ena   : in  std_logic;                      -- enable 
    Amn   : in  signed(width_in-1 downto 0);    -- Amn
    mA    : in  signed(width_in-1 downto 0);    -- mean of A
    Bmn   : in  signed(width_in-1 downto 0);    -- Bmn
    mB    : in  signed(width_in-1 downto 0);    -- mean of B
    Y     : out signed(2*width_in-1 downto 0);  -- Y
    XZ    : out signed(2*width_in-1 downto 0)   -- XZ    
    );

end entity calb;

architecture rtl of calb is

  signal AmnmA : signed (width_in-1 downto 0);
  signal BmnmB : signed (width_in-1 downto 0);

begin  -- architecture rtl

  signed_adder_subtractor_1 : entity work.signed_adder_subtractor
    generic map (
      DATA_WIDTH => width_in)
    port map (
      a       => Amn,
      b       => mA,
      add_sub => '0',
      ena => ena,
      result  => AmnmA);

  signed_adder_subtractor_2 : entity work.signed_adder_subtractor
    generic map (
      DATA_WIDTH => width_in)
    port map (
      a       => Bmn,
      b       => mB,
      add_sub => '0',
      ena => ena,
      result  => BmnmB);

  signed_multiply_accumulate_1 : entity work.signed_multiply_accumulate
    generic map (
      DATA_WIDTH => width_in)
    port map (
      a         => AmnmA,
      b         => BmnmB,
      clk       => clk,
      sload     => sload,
      accum_out => Y);

  signed_multiply_accumulate_2 : entity work.signed_multiply_accumulate
    generic map (
      DATA_WIDTH => width_in)
    port map (
      a         => BmnmB,
      b         => BmnmB,
      clk       => clk,
      sload     => sload,
      accum_out => XZ);

end architecture rtl;
