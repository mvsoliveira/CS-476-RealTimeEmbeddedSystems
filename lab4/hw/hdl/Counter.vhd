LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY Counter IS
	 PORT(
	 -- Avalon interface signals
		 Clk        : IN std_logic;
		 nReset     : IN std_logic;

	 -- Avalon Slave interfaces signals
		 S_Address    : IN std_logic_vector (1 DOWNTO 0);
		 S_ChipSelect : IN std_logic;

		 S_Read       : IN std_logic;
		 S_Write      : IN std_logic;

		 S_ReadData   : OUT std_logic_vector (31 DOWNTO 0);
		 S_WriteData  : IN std_logic_vector (31 DOWNTO 0);
		 
	 -- Interrupt line
		 IRQ			: OUT std_logic
	 );
End Counter;

ARCHITECTURE C_ARCH OF Counter IS

signal iRegCNT : std_logic_vector (31 DOWNTO 0) := (others => '0');

signal iRegCNT1 : std_logic_vector (31 DOWNTO 0) := (others => '0');

signal iRegCTL : std_logic_vector (4 DOWNTO 0) := (others => '0');

signal iRegST  : std_logic_vector (0 DOWNTO 0) := (others => '0');

signal TIM_OVF : std_logic;

BEGIN

-- Counting process
	pCount: process(Clk) begin
	if rising_edge(Clk) then
		-- tick
		if (iRegCTL(0) = '1') then
			iRegCNT <= std_logic_vector(unsigned(iRegCNT) + 1);
		end if;
		-- clear
		if (iRegCTL(1) = '1') then
			iRegCNT <= (others => '0');
			iRegCNT1 <= (others => '0');
		end if;
		-- set/reset overflow interrupt
		if (iRegCNT =  X"0000_0000") then
			TIM_OVF <= '1';
		elsif (iRegCTL(3) = '1') then
			TIM_OVF <= '0';
		end if;
		-- increment iRegCNT1 each 50th cycle (1u) 
		if (iRegCTL(4) = '1' and iRegCNT = X"0000_0031") then
			iRegCNT <= (others => '0');
			iRegCNT1 <= std_logic_vector(unsigned(iRegCNT1) + 1);
		end if;
	end if;
	end process pCount;

-- Timer overflow interrupt generation
	iRegST(0) <= '1' when iRegCTL(2) = '1' and TIM_OVF = '1' else '0';
	IRQ <= iRegST(0);

-- Avalon Slave read from registers process
	pRegRd: process(Clk) begin
	if rising_edge(Clk) then
		if S_ChipSelect = '1' and S_Read = '1' then -- Read cycle
				case S_Address(1 downto 0) is
					when "00" => S_ReadData <= std_logic_vector(resize(unsigned(iRegCTL), S_ReadData'length));
					when "01" => S_ReadData <= std_logic_vector(resize(unsigned(iRegST), S_ReadData'length));
					when "10" => S_ReadData <= iRegCNT;
					when "11" => S_ReadData <= iRegCNT1;
					when others => null;
				end case;
		end if;
	end if;
	end process pRegRd;

-- Avalon Slave write to registers process
	pRegWr: process(Clk, nReset) begin
		if nReset = '0' then
		-- Input by default
			iRegCTL <= (others => '0');
		elsif rising_edge(Clk) then
			iRegCTL(1) <= '0';
			iRegCTL(3) <= '0';
			if S_ChipSelect = '1' and S_Write = '1' then -- Write cycle
				case S_Address(1 downto 0) is
					when "00" => iRegCTL <= S_WriteData(4 DOWNTO 0);
					when others => null;
				end case;
			end if;
		end if;
	end process pRegWr;

END C_ARCH;
